package com.WifiProfiles;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.media.AudioManager;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.WifiProfiles.Domain.Profile;
import com.WifiProfiles.Receivers.ProfileChangedReceiver;
import com.WifiProfiles.Stores.ProfileStore;
import com.google.analytics.tracking.android.EasyTracker;

public class AddProfile extends Activity {
	
	public static final String mEXTRA_SSID = "ssid";
	public static final String mEXTRA_PROFILE = "profile";
	
	private EditText profileName;
	private SeekBar volBar;
	private CheckBox silentBox;
	private CheckBox vibrateBox;
	private SeekBar notiBar;
	private SeekBar mediaBar;
	private Button submit;
	private ProfileStore profileStore;
	private String mssid;
	private String mprofile;
	private boolean silent;
	private boolean vibrate;
	
	@Override
	protected void onStart() {
		super.onStart();
		
		EasyTracker.getInstance(this).activityStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		EasyTracker.getInstance(this).activityStop(this);
	}
	
	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		int titleID = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		
		TextView mAppName = (TextView) findViewById(titleID);
		mAppName.setTypeface(MainActivity.customFont);
		
		super.onCreate(savedInstanceState);
		
		setContentView(com.sechristfamily.WifiProfiles.R.layout.activity_addprofile);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		AudioManager am = (AudioManager) getSystemService(AUDIO_SERVICE);
		
		mssid =  (String)getIntent().getExtras().get(mEXTRA_SSID);
		mprofile = (String)getIntent().getExtras().getString(mEXTRA_PROFILE);
		
		profileStore = new ProfileStore(this);
		profileStore.open();
		
		silent = false;
		vibrate = false;
		
		profileName = (EditText) findViewById(com.sechristfamily.WifiProfiles.R.id.name);
		volBar = (SeekBar) findViewById(com.sechristfamily.WifiProfiles.R.id.ring_volume);
		silentBox = (CheckBox) findViewById(com.sechristfamily.WifiProfiles.R.id.silent);
		vibrateBox = (CheckBox) findViewById(com.sechristfamily.WifiProfiles.R.id.vibrate);
		notiBar = (SeekBar) findViewById(com.sechristfamily.WifiProfiles.R.id.noti_volume);
		mediaBar = (SeekBar) findViewById(com.sechristfamily.WifiProfiles.R.id.media_volume);
		submit = (Button) findViewById(com.sechristfamily.WifiProfiles.R.id.submit_profile);
		Button cancel = (Button) findViewById(com.sechristfamily.WifiProfiles.R.id.cancel_profile);
		
		volBar.setMax(am.getStreamMaxVolume(AudioManager.STREAM_RING));
		notiBar.setMax(am.getStreamMaxVolume(AudioManager.STREAM_NOTIFICATION));
		mediaBar.setMax(am.getStreamMaxVolume(AudioManager.STREAM_MUSIC));
		
		silentBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean arg1) {
				silent = !silent;
				volBar.setEnabled(!silent);
				notiBar.setEnabled(!silent);
				vibrateBox.setEnabled(!silent);
			}
		});
		
		vibrateBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				vibrate = !vibrate;
				volBar.setEnabled(!vibrate);
				notiBar.setEnabled(!vibrate);
				silentBox.setEnabled(!vibrate);
			}
		});
		
		if (mprofile == null) {
			
			Log.i("SSIDaddprofile", "SSID is " + mssid);
			
			submit.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					if (profileName.getText().toString().isEmpty()) {
						Toast.makeText(getApplicationContext(), "Profile name cannot be empty", Toast.LENGTH_SHORT).show();
						return;
					}
					Profile profile = new Profile();
					profile.setName(profileName.getText().toString());
					profile.setSilent(silent);
					profile.setVibrate(vibrate);
					profile.setRinger_vol(volBar.getProgress());
					profile.setNotification_vol(notiBar.getProgress());
					profile.setMedia_vol(mediaBar.getProgress());
					profile.setSSID(mssid);
					long id = profileStore.addProfile(profile);
					if (mssid.equals("no ssid")) {
						Intent intent = new Intent(AddProfile.this, MainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						
						Toast.makeText(AddProfile.this, "Profile " + profile.getName() + " Saved Successfully", Toast.LENGTH_SHORT).show();
					}
					else {
						profileStore.addSSID(profile, id);
						Intent intent = new Intent(AddProfile.this, MainActivity.class);
						intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
						startActivity(intent);
						Intent mintent = new Intent(getApplicationContext(), ProfileChangedReceiver.class);
						sendBroadcast(mintent);
						
						Toast.makeText(AddProfile.this, "Profile " + profile.getName() + " Linked to " + profile.getSSID() 
								+ " Successfully", Toast.LENGTH_SHORT).show();
					}
				}
			});
		}
		else {
			profileName.setText(mprofile);
			final Profile profile = profileStore.getProfilebyname(mprofile);
			volBar.setProgress(profile.getRinger_vol());
			silentBox.setChecked(profile.isSilent());
			vibrateBox.setChecked(profile.isVibrate());
			notiBar.setProgress(profile.getNotification_vol());
			mediaBar.setProgress(profile.getMedia_vol());
			
			if (profile.getRinger_vol() == 0 && profile.getNotification_vol() == 0 && !profile.isSilent())
				vibrateBox.setChecked(true);
			
			submit.setOnClickListener(new OnClickListener() {
				
				@Override
				public void onClick(View v) {
					long id = profileStore.getProfileID(profile);
					Profile newprofile = new Profile();
					newprofile.setName(profileName.getText().toString());
					newprofile.setRinger_vol(volBar.getProgress());
					newprofile.setSilent(silent);
					newprofile.setVibrate(vibrate);
					newprofile.setNotification_vol(notiBar.getProgress());
					newprofile.setMedia_vol(mediaBar.getProgress());
					profileStore.replaceProfile(newprofile, id);
					Intent mintent = new Intent(getApplicationContext(), ProfileChangedReceiver.class);
					sendBroadcast(mintent);
					finish();
					Toast.makeText(getApplicationContext(), "Profile " + newprofile.getName() + " Changed Successfully", Toast.LENGTH_SHORT).show();
				}
			});
		}
		
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		}
		return super.onOptionsItemSelected(item);
	}
}
