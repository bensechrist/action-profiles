package com.WifiProfiles;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.SharedPreferences.Editor;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Typeface;
import android.net.wifi.WifiConfiguration;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v4.view.ViewPager.OnPageChangeListener;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.WifiProfiles.Adapters.CustomTextArrayAdapter;
import com.WifiProfiles.Adapters.ProfileArrayAdapter;
import com.WifiProfiles.Backup.MyBackupAgent;
import com.WifiProfiles.Domain.Location;
import com.WifiProfiles.Domain.Profile;
import com.WifiProfiles.Receivers.AlarmReceiver;
import com.WifiProfiles.Receivers.ProfileChangedReceiver;
import com.WifiProfiles.Receivers.WifiStateChangeReceiver;
import com.WifiProfiles.Services.NotificationService;
import com.WifiProfiles.Stores.EventStore;
import com.WifiProfiles.Stores.GeofenceRemover;
import com.WifiProfiles.Stores.GeofenceRequester;
import com.WifiProfiles.Stores.LocationStore;
import com.WifiProfiles.Stores.ProfileStore;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.location.Geofence;
import com.sechristfamily.WifiProfiles.R;

public class MainActivity extends FragmentActivity {

	private ProfileStore profileStore;
	private EventStore eventStore;
	private LocationStore locationStore;
	private WifiManager wm;
	public static final String PrefsName = "com.WifiProfiles";
	public static final String ongoingNotificationPref = "com.WifiProfiles.ongoing";
	public static final String service_key = "com.WifiProfiles.service_enabler";
	public static final String new_ssid = "com.WifiProfiles.new_ssid";
	public static final String noti_enabled_key = "com.WifiProfiles.noti_enabled";
	public static Typeface customFont;
	
	private static final int NUM_PAGES = 3;
	public static final int LOCATION_REQUEST = 1;
	private static final int DATE_TIME_REQUEST = 2;
	public static final String NO_PROFILES = "No Profiles";
	public static String current_page_key = "CurrentPage";
	
	/**
	 * The {@link android.support.v4.view.PagerAdapter} that will provide
	 * fragments for each of the sections. We use a
	 * {@link android.support.v4.app.FragmentPagerAdapter} derivative, which
	 * will keep every loaded fragment in memory. If this becomes too memory
	 * intensive, it may be best to switch to a
	 * {@link android.support.v4.app.FragmentStatePagerAdapter}.
	 */
	SectionsPagerAdapter pagerAdapter;

	/**
	 * The {@link ViewPager} that will host the section contents.
	 */
	ViewPager pager;
	
	@Override
	protected void onStart() {
		super.onStart();
		
		EasyTracker.getInstance(this).activityStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		EasyTracker.getInstance(this).activityStop(this);
	}

	@SuppressLint("InlinedApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		profileStore = new ProfileStore(this);
		eventStore = new EventStore(this);
		locationStore = new LocationStore(this);
		
		customFont = Typeface.createFromAsset(getAssets(), "fonts/advent_bold.ttf");
		
		int titleID = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		
		TextView mAppName = (TextView) findViewById(titleID);
		mAppName.setTypeface(customFont);
		
		super.onCreate(savedInstanceState);
		
		wm = (WifiManager) getSystemService(WIFI_SERVICE);
		
		setContentView(com.sechristfamily.WifiProfiles.R.layout.activity_main);
		
		refreshPage(0);
		
		pager.setOnPageChangeListener(new OnPageChangeListener() {
			
			@Override
			public void onPageSelected(int arg0) {
				Log.i("CurPage", String.valueOf(arg0));
				SharedPreferences prefs = getSharedPreferences(PrefsName, 0);
				Editor editor = prefs.edit();
				editor.putInt(current_page_key, arg0);
				editor.commit();
			}
			
			@Override
			public void onPageScrolled(int arg0, float arg1, int arg2) {
			}
			
			@Override
			public void onPageScrollStateChanged(int arg0) {
			}
		});
	}
	
	@Override
	protected void onResume() {
		profileStore.open();
		eventStore.open();
		locationStore.open();

		SharedPreferences prefs = getSharedPreferences(MyBackupAgent.extra_shared_prefs, 0);
		if (prefs.getBoolean(MyBackupAgent.restored_geofences_key, false)) {
			GeofenceRequester requester = new GeofenceRequester(MainActivity.this);
			List<Geofence> geofences = new ArrayList<Geofence>();
			List<Location> locations = locationStore.getLocations();
			for (Location location : locations) {
				Geofence.Builder builder = new Geofence.Builder();
				builder.setCircularRegion(location.getLatitude(), location.getLongitude(), location.getRadius());
				builder.setExpirationDuration(LocationMap.geofenceExpiration);
				builder.setNotificationResponsiveness(LocationMap.geofenceResponsiveness);
				builder.setRequestId(location.getName());
				if ((location.getProfileEnterId() != -1) && (location.getProfileExitId() != -1))
					builder.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT);
				else if (location.getProfileEnterId() != -1)
					builder.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER);
				else if (location.getProfileExitId() != -1)
					builder.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_EXIT);
				geofences.add(builder.build());
			}
			
			requester.addGeofences(geofences);
			
			Editor editor = prefs.edit();
			editor.putBoolean(MyBackupAgent.restored_geofences_key, false);
			editor.commit();
		}
		
		SharedPreferences sp = getSharedPreferences(PrefsName, 0);
		refreshPage(sp.getInt(current_page_key, 0));
		
		String extra = "";
		if (getIntent().getStringExtra(NotificationService.notificationExtra) != null)
			extra = getIntent().getStringExtra(NotificationService.notificationExtra);
		Log.i("Extra", extra);
		if (extra.equals(AlarmReceiver.alarmNotiExtra))
			refreshPage(1);
		else if (extra.equals(WifiStateChangeReceiver.wifiNotiExtra))
			refreshPage(0);
		
		super.onResume();
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		switch (requestCode) {
		case LOCATION_REQUEST:
			refreshPage(2);
			break;
			
		case DATE_TIME_REQUEST:
			refreshPage(1);
			break;

		default:
			super.onActivityResult(requestCode, resultCode, data);
			break;
		}
	}
	
	@Override
	protected void onPause() {
		profileStore.close();
		eventStore.close();
		locationStore.close();
		super.onPause();
	}
	
	@Override
	public void onBackPressed() {
		if (pager.getCurrentItem() == 0)
			super.onBackPressed();
		else
			pager.setCurrentItem(pager.getCurrentItem() - 1);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(com.sechristfamily.WifiProfiles.R.menu.main, menu);
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		int id = item.getItemId();
		if (id == com.sechristfamily.WifiProfiles.R.id.add_router) {
			final String[] choices = { "Router", "Date and Time", "Location" };
			CustomTextArrayAdapter adapter = new CustomTextArrayAdapter(MainActivity.this,
					  android.R.layout.simple_list_item_1, android.R.id.text1, choices);
			AlertDialog.Builder chooseBuilder = new AlertDialog.Builder(MainActivity.this);
			chooseBuilder.setTitle("Implement Profile by:")
				.setSingleChoiceItems(adapter, 0, implimentChoice).show();
			
			return true;
		} else if (id == com.sechristfamily.WifiProfiles.R.id.edit_profiles) {
			final List<Profile> profiles = profileStore.getProfiles();
			profiles.add(new Profile() {
				
				@Override
				public String getName() {
					return "Add a new Profile";
				}
				
			});
			ProfileArrayAdapter profileAdapter = new ProfileArrayAdapter(this, profiles.toArray(new Profile[0]));
			AlertDialog.Builder editBuilder = new AlertDialog.Builder(MainActivity.this);
			editBuilder.setTitle("Select a Profile to Edit")
				.setSingleChoiceItems(profileAdapter, 0, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface arg0, int position) {
						arg0.dismiss();
						Profile clicked = profiles.get(position);
						Intent intent;
						if (clicked.getName().equals("Add a new Profile")) {
							intent = new Intent(MainActivity.this, AddProfile.class);
							intent.putExtra(AddProfile.mEXTRA_SSID, "no ssid");
							startActivity(intent);
						}
						else {
							intent = new Intent(MainActivity.this, AddProfile.class);
							intent.putExtra(AddProfile.mEXTRA_PROFILE, clicked.getName());
							startActivity(intent);
						}
					}
				}).show();
			return true;
		} else if (id == com.sechristfamily.WifiProfiles.R.id.delete_profile) {
			final List<Profile> profiles = profileStore.getProfiles();
			final ProfileArrayAdapter profileAdapter = new ProfileArrayAdapter(this, profiles.toArray(new Profile[0]));
			AlertDialog.Builder deleteBuilder = new AlertDialog.Builder(MainActivity.this);
			deleteBuilder.setTitle("Select Profiles to Remove")
				.setSingleChoiceItems(profileAdapter, 0, new DialogInterface.OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						GeofenceRemover remover = new GeofenceRemover(MainActivity.this);
						List<String> geofencesToRemove = new ArrayList<String>(
								locationStore.getLocationNamesByProfileId(profileStore.getProfileID(profiles.get(which))));
						if (!geofencesToRemove.isEmpty())
							remover.removeGeofencesById(geofencesToRemove);
						
						locationStore.removeLocationByProfileId(profileStore.getProfileID(profiles.get(which)));
						eventStore.removeEventbyProfileId(profileStore.getProfileID(profiles.get(which)));
						profileStore.removeProfile(profiles.get(which));
						
						Intent intent = new Intent(MainActivity.this, ProfileChangedReceiver.class);
						sendBroadcast(intent);

						Toast.makeText(MainActivity.this, profiles.get(which).getName() + " Deleted Successfully", Toast.LENGTH_SHORT).show();
						
						dialog.dismiss();
						
						refreshPage(0);
					}
				}).setNegativeButton("Cancel", null).show();
		} else if (id == com.sechristfamily.WifiProfiles.R.id.map) {
			Intent intent = new Intent(getApplicationContext(), LocationMap.class);
			startActivityForResult(intent, LOCATION_REQUEST);
		} else if (id == com.sechristfamily.WifiProfiles.R.id.settings) {
			Intent intent = new Intent(getApplicationContext(), SettingsMenuActivity.class);
			startActivity(intent);
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	DialogInterface.OnClickListener implimentChoice = new DialogInterface.OnClickListener() {
		
		CharSequence[] choices = { "Router", "Date and Time", "Location" };
		
		@Override
		public void onClick(DialogInterface arg0, int arg1) {
			arg0.dismiss();
			if (choices[arg1] == "Router") {
				AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
				LayoutInflater inflater = (LayoutInflater) getLayoutInflater();
				View view = inflater.inflate(R.layout.add_router_dialog, null);
				final Spinner routerSpinner = (Spinner) view.findViewById(R.id.router_spinner);
				final Spinner profileSpinner = (Spinner) view.findViewById(R.id.profile_spinner_add_router);
				builder.setView(view);
				builder.setTitle(R.string.add_router_dialog_title);
				builder.setPositiveButton(R.string.positive_button, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (profileSpinner.getSelectedItem().toString().equals(NO_PROFILES)) {
							Intent intent = new Intent(MainActivity.this, AddProfile.class);
							if (routerSpinner.getSelectedItem().toString() != null)
								intent.putExtra(AddProfile.mEXTRA_SSID, routerSpinner.getSelectedItem().toString());
							else
								intent.putExtra(AddProfile.mEXTRA_SSID, "no ssid");
							startActivity(intent);
							return;
						}
						Profile router = new Profile();
						router.setSSID(routerSpinner.getSelectedItem().toString());
						Profile selected = new Profile();
						selected.setName(profileSpinner.getSelectedItem().toString());
						profileStore.addSSID(router, profileStore.getProfileID(selected));
						Intent profileChangeIntent = new Intent(getApplicationContext(), ProfileChangedReceiver.class);
						sendBroadcast(profileChangeIntent);
						
						dialog.dismiss();
						
						refreshPage(0);
						
						Toast.makeText(MainActivity.this, "Router " + profileSpinner.getSelectedItem().toString() 
								+ " Linked to Profile " + selected.getName() + " Successfully", Toast.LENGTH_SHORT).show();
					}
				});
				builder.setNegativeButton(R.string.negative_button, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				
				List<String> ssids = new ArrayList<String>();
				if (!wm.isWifiEnabled())
					wm.setWifiEnabled(true);
				List<WifiConfiguration> results = wm.getConfiguredNetworks();
				for (WifiConfiguration result : results) {
					if (!profileStore.profileExistsforSSID(result.SSID.replace("\"", "")))
						ssids.add(result.SSID.replace("\"", ""));
				}
				if (ssids.isEmpty()) {
					Toast.makeText(MainActivity.this, "No More Routers Available to Add", Toast.LENGTH_SHORT).show();
					return;
				}
				ArrayAdapter<String> routers = 
						new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, ssids);
				routers.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				routerSpinner.setAdapter(routers);
				
				List<Profile> profiles = profileStore.getProfiles();
				List<String> profileNames = new ArrayList<String>();
				for (Profile profile : profiles) {
					profileNames.add(profile.getName());
				}
				if (profileNames.isEmpty())
					profileNames.add(NO_PROFILES);
				ArrayAdapter<String> profileAdapter = 
						new ArrayAdapter<String>(MainActivity.this, android.R.layout.simple_spinner_item, profileNames);
				profileAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				profileSpinner.setAdapter(profileAdapter);
				
				builder.show();
			} else if (choices[arg1] == "Date and Time") {
				Intent intent = new Intent(getApplicationContext(), AddDateTimeActivity.class);
				startActivityForResult(intent, DATE_TIME_REQUEST);
			} else if (choices[arg1] == "Location") {
				Intent intent = new Intent(getApplicationContext(), LocationMap.class);
				startActivityForResult(intent, LOCATION_REQUEST);
			}
		}
	};

	/**
	 * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
	 * one of the sections/tabs/pages.
	 */
	public class SectionsPagerAdapter extends FragmentPagerAdapter {
		
		public SectionsPagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			return MainFragment.create(position);
		}

		@Override
		public int getCount() {
			// Show total number of pages.
			return NUM_PAGES;
		}

		@Override
		public CharSequence getPageTitle(int position) {
			Locale l = Locale.getDefault();
			switch (position) {
			case 0:
				return getString(com.sechristfamily.WifiProfiles.R.string.tab_title1).toUpperCase(l);
			case 1:
				return getString(com.sechristfamily.WifiProfiles.R.string.tab_title2).toUpperCase(l);
			case 2:
				return getString(com.sechristfamily.WifiProfiles.R.string.tab_title3).toUpperCase(l);
			}
			return null;
		}
	}
	
	private void refreshPage(int pageToGoTo) {
		// Create the adapter that will return a fragment for each of the two
		// primary sections of the app.
		pagerAdapter = new SectionsPagerAdapter(getSupportFragmentManager());

		// Set up the ViewPager with the sections adapter.
		pager = (ViewPager) findViewById(com.sechristfamily.WifiProfiles.R.id.pager);
		pager.setAdapter(pagerAdapter);
		pager.setPageTransformer(true, new MyPageTransformer());
		pager.setCurrentItem(pageToGoTo);
	}
}