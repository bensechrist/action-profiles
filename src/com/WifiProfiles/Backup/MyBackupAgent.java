package com.WifiProfiles.Backup;

import java.io.IOException;

import android.app.backup.BackupAgentHelper;
import android.app.backup.BackupDataInput;
import android.app.backup.FileBackupHelper;
import android.app.backup.SharedPreferencesBackupHelper;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.ParcelFileDescriptor;

import com.WifiProfiles.MainActivity;
import com.WifiProfiles.Stores.MySQLiteHelper;

public class MyBackupAgent extends BackupAgentHelper {
	
	static final String dbStoreName = "db";
	static final String spStoreName = "sp";
	
	public static final String extra_shared_prefs = "com.WifiProfiles.extra_sp";
	public static final String restored_geofences_key = "com.WifiProfiles.restored_geofences";

	@Override
	public void onCreate() {
		FileBackupHelper db = new FileBackupHelper(this,
			    "../databases/" + MySQLiteHelper.DATABASE_NAME);
		addHelper(dbStoreName, db);
		
		SharedPreferencesBackupHelper sp = 
				new SharedPreferencesBackupHelper(this, MainActivity.PrefsName);
		addHelper(spStoreName, sp);
	}
	
	@Override
	public void onRestore(BackupDataInput data, int appVersionCode,
			ParcelFileDescriptor newState) throws IOException {
		super.onRestore(data, appVersionCode, newState);
		
		SharedPreferences prefs = getSharedPreferences(extra_shared_prefs, 0);
		Editor editor = prefs.edit();
		editor.putBoolean(restored_geofences_key, true);
		editor.commit();
	}
}
