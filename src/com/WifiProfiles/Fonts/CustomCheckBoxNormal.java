package com.WifiProfiles.Fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.CheckBox;
import com.WifiProfiles.MainActivity;

public class CustomCheckBoxNormal extends CheckBox {

	@SuppressLint("Instantiatable")
	public CustomCheckBoxNormal(Context context) {
        super(context);
    }

    public CustomCheckBoxNormal(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(MainActivity.customFont);
    }

    public CustomCheckBoxNormal(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(MainActivity.customFont);
    }
    
}
