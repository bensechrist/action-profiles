package com.WifiProfiles.Fonts;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.TextView;
import com.WifiProfiles.MainActivity;

public class CustomTextViewNormal extends TextView {
	
	public CustomTextViewNormal(Context context)
    {
        super(context);
        init();
    }

    public CustomTextViewNormal(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    @SuppressLint("Instantiatable")
	public CustomTextViewNormal(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
    }

    public void init()
    {
        setTypeface(MainActivity.customFont);
    }
    
}
