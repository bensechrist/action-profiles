package com.WifiProfiles.Fonts;

import com.WifiProfiles.MainActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.EditText;

public class CustomEditTextNormal extends EditText{
	
	public CustomEditTextNormal(Context context)
    {
        super(context);
        init();
    }

    public CustomEditTextNormal(Context context, AttributeSet attrs)
    {
        super(context, attrs);
        init();
    }

    @SuppressLint("Instantiatable")
	public CustomEditTextNormal(Context context, AttributeSet attrs, int defStyle)
    {
        super(context, attrs, defStyle);
        init();
    }

    public void init()
    {
        setTypeface(MainActivity.customFont);
    }
}
