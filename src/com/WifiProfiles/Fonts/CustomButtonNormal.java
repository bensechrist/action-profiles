package com.WifiProfiles.Fonts;

import com.WifiProfiles.MainActivity;
import android.annotation.SuppressLint;
import android.content.Context;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomButtonNormal extends Button {

	@SuppressLint("Instantiatable")
	public CustomButtonNormal(Context context) {
        super(context);
    }

    public CustomButtonNormal(Context context, AttributeSet attrs) {
        super(context, attrs);
        setTypeface(MainActivity.customFont);
    }

    public CustomButtonNormal(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        setTypeface(MainActivity.customFont);
    }
    
    
}
