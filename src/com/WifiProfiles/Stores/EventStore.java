package com.WifiProfiles.Stores;

import java.util.ArrayList;
import java.util.List;

import android.app.backup.BackupManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.WifiProfiles.Domain.Event;

public class EventStore {

	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allEventcolumns = { MySQLiteHelper.COLUMN_TIME,
			MySQLiteHelper.COLUMN_EVENT_IDTOPROFILE, MySQLiteHelper.COLUMN_ALARM_ID,
			MySQLiteHelper.COLUMN_RECUR };
	private BackupManager backupMng;
	
	public EventStore(Context context) {
		dbHelper = new MySQLiteHelper(context);
		backupMng = new BackupManager(context);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public int addEvent(long timeInMills, long idToProfile, String recurring) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_TIME, timeInMills);
		values.put(MySQLiteHelper.COLUMN_EVENT_IDTOPROFILE, idToProfile);
		values.put(MySQLiteHelper.COLUMN_RECUR, recurring);
		long alarmId = database.insert(MySQLiteHelper.TABLE_EVENTS, null, values);
		
		backupMng.dataChanged();
		return (int) alarmId;
	}
	
	public List<Event> getEvents() {
		List<Event> events = new ArrayList<Event>();
		
		Cursor cursor = database.query(MySQLiteHelper.TABLE_EVENTS, allEventcolumns, null, null, null, null, MySQLiteHelper.COLUMN_TIME + " ASC");
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Event event = cursorToEvent(cursor);
			events.add(event);
			cursor.moveToNext();
		}
		return events;
	}
	
	public Event getEvent(long id) {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_EVENTS, allEventcolumns, MySQLiteHelper.COLUMN_ALARM_ID + "=?", new String[]{String.valueOf(id)}, null, null, null);
		cursor.moveToFirst();
		if (cursor.getCount() == 0)
			return null;
		return cursorToEvent(cursor);
	}
	
	public int getAlarmID(long timeInMills) {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_EVENTS, allEventcolumns, MySQLiteHelper.COLUMN_TIME + "=?", new String[]{String.valueOf(timeInMills)}, null, null, null);
		cursor.moveToFirst();
		if (cursor.getCount() == 0)
			return -1;
		
		return cursor.getInt(2);
	}
	
	public void removeEvent(int alarmId) {
		database.delete(MySQLiteHelper.TABLE_EVENTS, MySQLiteHelper.COLUMN_ALARM_ID + "=" + "?", new String[]{String.valueOf(alarmId)});
		backupMng.dataChanged();
	}
	
	public void removeEventbyProfileId(long profileId) {
		database.delete(MySQLiteHelper.TABLE_EVENTS, MySQLiteHelper.COLUMN_EVENT_IDTOPROFILE + "=?", new String[]{String.valueOf(profileId)});
		backupMng.dataChanged();
	}
	
	private Event cursorToEvent(Cursor cursor) {
		Event event = new Event();
		event.setEventTimeInMills(cursor.getLong(0));
		event.setIdToProfile(cursor.getInt(1));
		event.setAlarmID(cursor.getInt(2));
		event.setRecurring(cursor.getString(3));
		return event;
	}
}
