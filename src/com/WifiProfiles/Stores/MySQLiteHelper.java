package com.WifiProfiles.Stores;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class MySQLiteHelper extends SQLiteOpenHelper {
	
	public static final String DATABASE_NAME = "wifiprofiles";
	private static final int DATABASE_VERSION = 4;

	/***************** PROFILE TABLE SETUP ******************/
	public static final String TABLE_PROFILE = "wifiprofiles";
	public static final String COLUMN_NAME = "name";
	public static final String COLUMN_ID = "_id";
	public static final String COLUMN_RING = "ringer_vol";
	public static final String COLUMN_NOTI = "noti_vol";
	public static final String COLUMN_MEDIA = "media_vol";
	public static final String COLUMN_SILENT = "silent";
	public static final String COLUMN_VIBRATE = "vibrate";

	private static final String TABLE_PROFILE_CREATE = 
		"create table " + TABLE_PROFILE + "(" 
			+ COLUMN_ID + " integer primary key autoincrement, "
			+ COLUMN_NAME + " text not null, " + COLUMN_RING 
			+ " integer, " + COLUMN_NOTI + " integer, " 
			+ COLUMN_MEDIA + " integer, " + COLUMN_SILENT
			+ " integer, " + COLUMN_VIBRATE + " integer);";

	
	/***************** SSID TABLE SETUP ******************/
	public static final String TABLE_SSID = "wifissids";
	public static final String COLUMN_SSID = "ssid";
	public static final String COLUMN_IDTOPROFILE = "profileid";

	private static final String TABLE_SSID_CREATE = 
		"create table " + TABLE_SSID + "(" + COLUMN_SSID
			+ " text primary key not null, " + COLUMN_IDTOPROFILE 
			+ " integer);";
	
	/***************** EVENT TABLE SETUP ******************/
	public static final String TABLE_EVENTS = "events";
	public static final String COLUMN_TIME = "time";
	public static final String COLUMN_EVENT_IDTOPROFILE = "eventprofileid";
	public static final String COLUMN_ALARM_ID = "alarmid";
	public static final String COLUMN_RECUR = "recurring";

	private static final String TABLE_EVENT_CREATE = 
		"create table " + TABLE_EVENTS + "(" + COLUMN_TIME
			+ " long not null, " + COLUMN_EVENT_IDTOPROFILE
			+ " integer, " + COLUMN_ALARM_ID + " integer primary key autoincrement, "
			+ COLUMN_RECUR + " text);";
	
	/***************** PROFILE TABLE SETUP ******************/
	public static final String TABLE_ZONES = "zones";
	public static final String COLUMN_ZONE_NAME = "zonename";
	public static final String COLUMN_LATITUDE = "latitude";
	public static final String COLUMN_LONGITUDE = "longitude";
	public static final String COLUMN_RADIUS = "radius";
	public static final String COLUMN_PROFILE_ENTER_ID = "profileenterid";
	public static final String COLUMN_PROFILE_EXIT_ID = "profileexitid";

	private static final String TABLE_ZONES_CREATE = 
		"create table " + TABLE_ZONES + "(" 
			+ COLUMN_ZONE_NAME + " text primary key not null, "
			+ COLUMN_LATITUDE + " text not null, " + COLUMN_LONGITUDE + " text not null, "
			+ COLUMN_RADIUS + " float not null, " + COLUMN_PROFILE_ENTER_ID  + " integer not null, "
			+ COLUMN_PROFILE_EXIT_ID + " integer not null);";
		
	public MySQLiteHelper(Context context) {
		super(context, DATABASE_NAME, null, DATABASE_VERSION);
	}
	
	@Override
	public void onCreate(SQLiteDatabase database) {
		database.execSQL(TABLE_PROFILE_CREATE);
		database.execSQL(TABLE_SSID_CREATE);
		database.execSQL(TABLE_EVENT_CREATE);
		database.execSQL(TABLE_ZONES_CREATE);
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.w(MySQLiteHelper.class.getName(), "Upgrading database from version " + oldVersion + " to " + newVersion + ", which will add a new column");
		
		db.execSQL("ALTER TABLE " + TABLE_PROFILE + " ADD COLUMN " + COLUMN_VIBRATE + " INTEGER");
		
		Cursor cursor = db.query(TABLE_PROFILE, ProfileStore.allProfilecolumns, null,
				null, null, null, null);
		cursor.moveToFirst();
		
		while (!cursor.isAfterLast()) {
			ContentValues values = new ContentValues();
			if ((cursor.getInt(2) == 0) && (cursor.getInt(5) == 0))
				values.put(COLUMN_VIBRATE, 1);
			else
				values.put(COLUMN_VIBRATE, 0);
			db.update(TABLE_PROFILE, values, COLUMN_ID + "=?", new String[]{ String.valueOf(cursor.getInt(0)) });
			
			cursor.moveToNext();
		}
		cursor.close();
	}
	
}
