package com.WifiProfiles.Stores;

import java.util.ArrayList;
import java.util.List;

import android.app.backup.BackupManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.WifiProfiles.Domain.Profile;

public class ProfileStore {
	
	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	public static String[] allProfilecolumns = { MySQLiteHelper.COLUMN_ID, 
			MySQLiteHelper.COLUMN_NAME, MySQLiteHelper.COLUMN_RING, 
			MySQLiteHelper.COLUMN_NOTI, MySQLiteHelper.COLUMN_MEDIA,
			MySQLiteHelper.COLUMN_SILENT, MySQLiteHelper.COLUMN_VIBRATE };
	private String[] allSSIDcolumns = { MySQLiteHelper.COLUMN_SSID, 
			MySQLiteHelper.COLUMN_IDTOPROFILE };
	private BackupManager backupMng;
	
	public ProfileStore(Context context) {
		dbHelper = new MySQLiteHelper(context);
		backupMng = new BackupManager(context);
	}
	
	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}
	
	public void close() {
		dbHelper.close();
	}
	
	public long addProfile(Profile profile) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_NAME, profile.getName());
		values.put(MySQLiteHelper.COLUMN_RING, profile.getRinger_vol());
		values.put(MySQLiteHelper.COLUMN_NOTI, profile.getNotification_vol());
		values.put(MySQLiteHelper.COLUMN_MEDIA, profile.getMedia_vol());
		values.put(MySQLiteHelper.COLUMN_SILENT, profile.isSilent());
		values.put(MySQLiteHelper.COLUMN_VIBRATE, profile.isVibrate());
		long insertId = database.insert(MySQLiteHelper.TABLE_PROFILE, null, values);
		
		backupMng.dataChanged();
		return insertId;
	}
	
	public void replaceProfile(Profile profile, long id) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_NAME, profile.getName());
		values.put(MySQLiteHelper.COLUMN_SILENT, profile.isSilent());
		values.put(MySQLiteHelper.COLUMN_VIBRATE, profile.isVibrate());
		values.put(MySQLiteHelper.COLUMN_RING, profile.getRinger_vol());
		values.put(MySQLiteHelper.COLUMN_NOTI, profile.getNotification_vol());
		values.put(MySQLiteHelper.COLUMN_MEDIA, profile.getMedia_vol());
		values.put(MySQLiteHelper.COLUMN_ID, id);
		database.update(MySQLiteHelper.TABLE_PROFILE, values, MySQLiteHelper.COLUMN_ID + "=?", new String[]{ String.valueOf(id) });
		
		backupMng.dataChanged();
	}
	
	public void addSSID(Profile profile, long id) {
		ContentValues ssidvalues = new ContentValues();
		ssidvalues.put(MySQLiteHelper.COLUMN_SSID, profile.getSSID());
		ssidvalues.put(MySQLiteHelper.COLUMN_IDTOPROFILE, id);
		database.insert(MySQLiteHelper.TABLE_SSID, null, ssidvalues);
		
		backupMng.dataChanged();
	}
	
	public List<Profile> getProfiles() {
		List<Profile> profiles = new ArrayList<Profile>();
		
		Cursor cursor = database.query(MySQLiteHelper.TABLE_PROFILE, 
				allProfilecolumns, null, null, null, null, 
				null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Profile profile = cursorToProfile(cursor);
			profiles.add(profile);
			cursor.moveToNext();
		}
		cursor.close();
		return profiles;
	}
	
	public List<String> getSSIDs() {
		List<String> ssids = new ArrayList<String>();
		
		Cursor cursor = database.query(MySQLiteHelper.TABLE_SSID, allSSIDcolumns, null, null, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			String ssid = cursorToSSID(cursor);
			ssids.add(ssid);
			cursor.moveToNext();
		}
		return ssids;
	}
	
	private Profile cursorToProfile(Cursor cursor) {
		Profile profile = new Profile();
		profile.setId(cursor.getInt(0));
		profile.setName(cursor.getString(1));
		profile.setRinger_vol(cursor.getInt(2));
		profile.setNotification_vol(cursor.getInt(3));
		profile.setMedia_vol(cursor.getInt(4));
		int silent = cursor.getInt(5);
		if (silent == 1)
			profile.setSilent(true);
		else
			profile.setSilent(false);
		int vibrate = cursor.getInt(6);
		if (vibrate == 1)
			profile.setVibrate(true);
		else
			profile.setVibrate(false);
		return profile;
	}
	
	private String cursorToSSID(Cursor cursor) {
		String ssid = cursor.getString(0);
		return ssid;
	}
	
	private int cursorToProfileID(Cursor cursor) {
		return cursor.getInt(1);
	}
	
	public Profile getProfilebyname(String name) {
		Profile profile = new Profile();
		
		Cursor cursor = database.query(MySQLiteHelper.TABLE_PROFILE, allProfilecolumns, MySQLiteHelper.COLUMN_NAME + "=" + "?", new String[]{name}, null, null, null);
		
		cursor.moveToFirst();
		profile = cursorToProfile(cursor);
		return profile;
	}
	
	public Profile getProfilebySSID(String ssid) {
		Profile profile = new Profile();
		
		Cursor cursor = database.query(MySQLiteHelper.TABLE_SSID, allSSIDcolumns, MySQLiteHelper.COLUMN_SSID + "=" + "?", new String[]{ssid}, null, null, null);
		
		cursor.moveToFirst();
		int id = cursorToProfileID(cursor);
		
		Cursor mcursor = database.query(MySQLiteHelper.TABLE_PROFILE, allProfilecolumns, MySQLiteHelper.COLUMN_ID + "=" + "?", new String[]{Integer.toString(id)}, null, null, null);
		
		mcursor.moveToFirst();
		profile = cursorToProfile(mcursor);
		
		return profile;
	}
	
	public Profile getProfilebyID(long id) {
		Profile profile = new Profile();
		
		Cursor cursor = database.query(MySQLiteHelper.TABLE_PROFILE, allProfilecolumns, MySQLiteHelper.COLUMN_ID + "=" + "?", new String[]{String.valueOf(id)}, null, null, null);
		
		cursor.moveToFirst();
		profile = cursorToProfile(cursor);
		
		return profile;
	}
	
	public long getProfileID(Profile profile) {
		long id;
		Cursor cursor = database.query(MySQLiteHelper.TABLE_PROFILE, allProfilecolumns, MySQLiteHelper.COLUMN_NAME + "=?", new String[]{profile.getName()}, null, null, null);
		cursor.moveToFirst();
		Profile mprofile = cursorToProfile(cursor);
		id = mprofile.getId();
		return id;
	}
	
	public boolean profileExistsforSSID(String SSID) {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_SSID, allSSIDcolumns, MySQLiteHelper.COLUMN_SSID + "=?", new String[]{SSID}, null, null, null);
		cursor.moveToLast();
		int count = cursor.getCount();
		if (count == 0) {
			Log.i("false", "no profile exists");
			return false;
		}
		else {
			Log.i("true", "profile exists");
			return true;
		}
	}
	
	public void removeProfile(Profile profile) {
		long profileid = getProfileID(profile);
		removeAssocSSIDs(profileid);
		
		database.delete(MySQLiteHelper.TABLE_PROFILE, MySQLiteHelper.COLUMN_NAME + "=?", new String[]{profile.getName()});
		
		Log.i("deleted", "Deleted " + profile.getName());
		
		backupMng.dataChanged();
	}
	
	public void removeAssocSSIDs(long profileid) {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_SSID, allSSIDcolumns, MySQLiteHelper.COLUMN_IDTOPROFILE + "=?", new String[]{String.valueOf(profileid)}, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			String ssid = cursorToSSID(cursor);
			removeSSID(ssid);
			cursor.moveToNext();
		}
		
		backupMng.dataChanged();
	}
	
	public void removeSSID(String SSID) {
		database.delete(MySQLiteHelper.TABLE_SSID, MySQLiteHelper.COLUMN_SSID + "=" + "?", new String[]{SSID});
		Log.i("deleted", "Deleted " + SSID);
		
		backupMng.dataChanged();
	}
	
}
