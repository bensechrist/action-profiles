package com.WifiProfiles.Stores;

import java.util.ArrayList;
import java.util.List;

import com.WifiProfiles.Domain.Location;

import android.app.backup.BackupManager;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

public class LocationStore {

	private SQLiteDatabase database;
	private MySQLiteHelper dbHelper;
	private String[] allZonecolumns = { MySQLiteHelper.COLUMN_ZONE_NAME, 
			MySQLiteHelper.COLUMN_LATITUDE, 
			MySQLiteHelper.COLUMN_LONGITUDE,
			MySQLiteHelper.COLUMN_RADIUS,
			MySQLiteHelper.COLUMN_PROFILE_ENTER_ID,
			MySQLiteHelper.COLUMN_PROFILE_EXIT_ID};
	private BackupManager backupMng;

	public LocationStore(Context context) {
		dbHelper = new MySQLiteHelper(context);
		backupMng = new BackupManager(context);
	}

	public void open() throws SQLException {
		database = dbHelper.getWritableDatabase();
	}

	public void close() {
		dbHelper.close();
	}

	public long addLocation(Location location) {
		Log.i("Add Location", location.getName() + " " + location.getLatitude() + " " + location.getLongitude() + " " + location.getRadius());
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_ZONE_NAME, location.getName());
		values.put(MySQLiteHelper.COLUMN_LATITUDE, location.getLatitude());
		values.put(MySQLiteHelper.COLUMN_LONGITUDE, location.getLongitude());
		values.put(MySQLiteHelper.COLUMN_RADIUS, location.getRadius());
		values.put(MySQLiteHelper.COLUMN_PROFILE_ENTER_ID, location.getProfileEnterId());
		values.put(MySQLiteHelper.COLUMN_PROFILE_EXIT_ID, location.getProfileExitId());
		long insertId = database.insert(MySQLiteHelper.TABLE_ZONES, null, values);
		
		backupMng.dataChanged();
		return insertId;
	}

	public void replaceLocation(Location location) {
		ContentValues values = new ContentValues();
		values.put(MySQLiteHelper.COLUMN_ZONE_NAME, location.getName());
		values.put(MySQLiteHelper.COLUMN_LATITUDE, location.getLatitude());
		values.put(MySQLiteHelper.COLUMN_LONGITUDE, location.getLongitude());
		values.put(MySQLiteHelper.COLUMN_RADIUS, location.getRadius());
		values.put(MySQLiteHelper.COLUMN_PROFILE_ENTER_ID, location.getProfileEnterId());
		values.put(MySQLiteHelper.COLUMN_PROFILE_EXIT_ID, location.getProfileExitId());
		database.delete(MySQLiteHelper.TABLE_ZONES, MySQLiteHelper.COLUMN_ZONE_NAME + "=?", new String[]{location.getName()});
		database.insert(MySQLiteHelper.TABLE_ZONES, null, values);
		
		backupMng.dataChanged();
	}

	public List<Location> getLocations() {
		List<Location> zones = new ArrayList<Location>();

		Cursor cursor = database.query(MySQLiteHelper.TABLE_ZONES, 
				allZonecolumns, null, null, null, null, 
				null);
		
		cursor.moveToFirst();
		Log.i("Cursor Size", String.valueOf(cursor.getCount()));
		while (!cursor.isAfterLast()) {
			Location temp = cursorToLocation(cursor);
			Log.i("All Locations", temp.getName());
			zones.add(temp);
			cursor.moveToNext();
		}
		cursor.close();
		return zones;
	}
	
	public List<String> getLocationNamesByProfileId(long ProfileId) {
		List<String> locationNames = new ArrayList<String>();
		
		Cursor cursor = database.query(MySQLiteHelper.TABLE_ZONES,
				allZonecolumns, MySQLiteHelper.COLUMN_PROFILE_ENTER_ID + "=? OR " + MySQLiteHelper.COLUMN_PROFILE_EXIT_ID + "=?", 
				new String[]{String.valueOf(ProfileId), String.valueOf(ProfileId)}, null, null, null);
		
		cursor.moveToFirst();
		while (!cursor.isAfterLast()) {
			Location temp = cursorToLocation(cursor);
			locationNames.add(temp.getName());
			cursor.moveToNext();
		}
		cursor.close();
		return locationNames;
	}
	
	public Location getLocation(String name) {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_ZONES, allZonecolumns, MySQLiteHelper.COLUMN_ZONE_NAME + " =?", new String[]{name}, null, null, null);
		
		if (cursor.getCount() == 1) {
			cursor.moveToFirst();
			Location location = cursorToLocation(cursor);
			cursor.close();
			return location;
		} else
			return null;
	}
	
	public boolean locationExists(String name) {
		Cursor cursor = database.query(MySQLiteHelper.TABLE_ZONES, allZonecolumns, MySQLiteHelper.COLUMN_ZONE_NAME + " =?", new String[]{name}, null, null, null);
		
		if (cursor.getCount() >= 1)
			return true;
		else
			return false;
	}

	public void removeLocation(Location location) {
		Log.i("Location", "Removing location " + location.getName());

		database.delete(MySQLiteHelper.TABLE_ZONES, 
				MySQLiteHelper.COLUMN_LATITUDE + "=? AND " + MySQLiteHelper.COLUMN_LONGITUDE + "=? AND " + MySQLiteHelper.COLUMN_ZONE_NAME + "=?", 
				new String[]{String.valueOf(location.getLatitude()), String.valueOf(location.getLongitude()), location.getName()});

		Log.i("deleted", "Deleted zone");
		
		backupMng.dataChanged();
	}
	
	public void removeLocationByProfileId(long profileId) {
		database.delete(MySQLiteHelper.TABLE_ZONES, MySQLiteHelper.COLUMN_PROFILE_ENTER_ID + "=? OR " + MySQLiteHelper.COLUMN_PROFILE_EXIT_ID + "=?",
				new String[]{String.valueOf(profileId), String.valueOf(profileId)});
		
		backupMng.dataChanged();
	}
	
	public void removeAllLocations() {
		database.delete(MySQLiteHelper.TABLE_ZONES, null, null);
		
		backupMng.dataChanged();
	}
	
	private Location cursorToLocation(Cursor cursor) {
		Location location = new Location();
		location.setName(cursor.getString(0));
		location.setLatitude(cursor.getDouble(1));
		location.setLongitude(cursor.getDouble(2));
		location.setRadius(cursor.getFloat(3));
		location.setProfileEnterId(cursor.getInt(4));
		location.setProfileExitId(cursor.getInt(5));
		return location;
	}
}
