package com.WifiProfiles;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import android.app.ActionBar;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.res.Resources;
import android.graphics.Color;
import android.location.Criteria;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.WifiProfiles.Adapters.LocationArrayAdapter;
import com.WifiProfiles.Adapters.MapPopupAdapter;
import com.WifiProfiles.Domain.Location;
import com.WifiProfiles.Domain.Profile;
import com.WifiProfiles.Receivers.ProfileChangedReceiver;
import com.WifiProfiles.Stores.GeofenceRemover;
import com.WifiProfiles.Stores.GeofenceRequester;
import com.WifiProfiles.Stores.LocationStore;
import com.WifiProfiles.Stores.ProfileStore;
import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.GoogleMap.OnInfoWindowClickListener;
import com.google.android.gms.maps.GoogleMap.OnMapLongClickListener;
import com.google.android.gms.maps.GoogleMap.OnMarkerDragListener;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.Circle;
import com.google.android.gms.maps.model.CircleOptions;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.sechristfamily.WifiProfiles.R;

public class LocationMap extends Activity implements
			OnInfoWindowClickListener,
			OnMarkerDragListener {
	
	private LocationManager locationManager;
	private String provider;
	private GoogleMap map;
	private Location location;
	private LocationStore locationStore;
	private ProfileStore profileStore;
	private GeofenceRequester geofenceRequester;
	private GeofenceRemover geofenceRemover;
	
	private Spinner enterProfileSpinner;
	private Spinner exitProfileSpinner;
	private Spinner radiusSpinner;
	private EditText geofenceTitle;
	private CheckBox enterCheckBox;
	private CheckBox exitCheckBox;
	
	private HashMap<Marker, Circle> markerCircleMap;
	
	private final int fillColor = Color.argb(0x80, 0xff, 0xff, 00);
	private final int strokeColor = Color.TRANSPARENT;
	private final float strokeWidth = 4;
	public static final int geofenceResponsiveness = 0;
	public static final long geofenceExpiration = Geofence.NEVER_EXPIRE;
	private final float RADIUS_SMALL = 50;
	private final float RADIUS_MEDIUM = 100;
	private final float RADIUS_LARGE = 150;
	private final int DEFAULT_ZOOM = 16;
	private final String NO_LOCATIONS = "Long Press Map to Add a Location"; 
	
	@Override
	protected void onStart() {
		super.onStart();
		
		EasyTracker.getInstance(this).activityStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		EasyTracker.getInstance(this).activityStop(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		int titleID = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		
		TextView mAppName = (TextView) findViewById(titleID);
		mAppName.setTypeface(MainActivity.customFont);
		
		super.onCreate(savedInstanceState);
		
		markerCircleMap = new HashMap<Marker, Circle>();
		
		profileStore = new ProfileStore(this);
		
		locationStore = new LocationStore(this);
		locationStore.open();
		
		geofenceRemover = new GeofenceRemover(this);
		geofenceRequester = new GeofenceRequester(this);
		
		setContentView(R.layout.map_layout);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		Button removeAll = (Button) findViewById(R.id.remove_all_geofences);
		removeAll.setOnClickListener(new View.OnClickListener() {
			
			@Override
			public void onClick(View v) {
				AlertDialog.Builder confirm = new AlertDialog.Builder(LocationMap.this);
				confirm.setMessage(R.string.confirm_delete_all);
				confirm.setPositiveButton("Yes", new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						geofenceRemover.removeGeofencesByIntent(geofenceRequester.getRequestPendingIntent());
						
						locationStore.removeAllLocations();
						
						Iterator<Map.Entry<Marker, Circle>> it = markerCircleMap.entrySet().iterator();
						while (it.hasNext()) {
							Map.Entry<Marker, Circle> pairs = it.next();
							Marker marker = pairs.getKey();
							Circle circle = pairs.getValue();
							marker.remove();
							circle.remove();
						} 
						
						markerCircleMap.clear();
						
						Toast.makeText(LocationMap.this, "All Locations Deleted Successfully", Toast.LENGTH_SHORT).show();
					}
				});
				confirm.setNegativeButton("No", new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
				confirm.show();
			}
			
		});
		
		map = ((MapFragment) getFragmentManager().findFragmentById(R.id.map))
		        .getMap();
		map.setMyLocationEnabled(true);
		map.setOnMapLongClickListener(longClickListener());
		map.setInfoWindowAdapter(new MapPopupAdapter(this));
		map.setOnInfoWindowClickListener(this);
		map.setOnMarkerDragListener(this);
		
		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		
		Criteria criteria = new Criteria();
		provider = locationManager.getBestProvider(criteria, false);
		
		android.location.Location currentLocation = locationManager.getLastKnownLocation(provider);
		if (currentLocation != null)
			map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()), DEFAULT_ZOOM));
	}
	
	@Override
	public void onInfoWindowClick(final Marker marker) {
		location = locationStore.getLocation(marker.getTitle());
		
		AlertDialog.Builder builder = new AlertDialog.Builder(LocationMap.this);
		LayoutInflater inflator = (LayoutInflater) getLayoutInflater();
		View view = inflator.inflate(R.layout.location_add_dialog, null);
		radiusSpinner = (Spinner) view.findViewById(R.id.radius_spinner);
		enterProfileSpinner = (Spinner) view.findViewById(R.id.enter_profile);
		exitProfileSpinner = (Spinner) view.findViewById(R.id.exit_profile);
		geofenceTitle = (EditText) view.findViewById(R.id.location_title);
		enterCheckBox = (CheckBox) view.findViewById(R.id.enter_profile_check);
		exitCheckBox = (CheckBox) view.findViewById(R.id.exit_profile_check);
		builder.setView(view);
		builder.setTitle(R.string.edit_dialog_title);
		builder.setPositiveButton(R.string.positive_button, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				location.setLatitude(marker.getPosition().latitude);
				location.setLongitude(marker.getPosition().longitude);
				Geofence.Builder geoBuilder = new Geofence.Builder();
				
				String radiusText = radiusSpinner.getSelectedItem().toString();
				float radius = 0;
				if (radiusText.equals("Small"))
					radius = RADIUS_SMALL;
				else if (radiusText.equals("Medium"))
					radius = RADIUS_MEDIUM;
				else if (radiusText.equals("Large"))
					radius = RADIUS_LARGE;
				
				location.setRadius(radius);
				
				geoBuilder.setCircularRegion(marker.getPosition().latitude, marker.getPosition().longitude, radius);
				geoBuilder.setExpirationDuration(geofenceExpiration);
				geoBuilder.setNotificationResponsiveness(geofenceResponsiveness);
				
				String title = geofenceTitle.getText().toString();
				if (title.isEmpty() || title == null) {
					Toast.makeText(LocationMap.this, "Title cannot be Empty", Toast.LENGTH_SHORT).show();
					return;
				}
				
				Log.i("LocationName", title);
				location.setName(title);
				
				geoBuilder.setRequestId(title);
				
				int transitionType = 0;
				if (enterCheckBox.isChecked() && exitCheckBox.isChecked()) {
					transitionType = Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT;
					location.setProfileEnterId((int) profileStore.getProfileID(
							profileStore.getProfilebyname(enterProfileSpinner.getSelectedItem().toString())));
					location.setProfileExitId((int) profileStore.getProfileID(
							profileStore.getProfilebyname(exitProfileSpinner.getSelectedItem().toString())));
				} else if (exitCheckBox.isChecked() && !enterCheckBox.isChecked()) {
					transitionType = Geofence.GEOFENCE_TRANSITION_EXIT;
					location.setProfileExitId((int) profileStore.getProfileID(
							profileStore.getProfilebyname(exitProfileSpinner.getSelectedItem().toString())));
					location.setProfileEnterId(-1);
				} else if (enterCheckBox.isChecked() && !exitCheckBox.isChecked()) {
					transitionType = Geofence.GEOFENCE_TRANSITION_ENTER;
					location.setProfileEnterId((int) profileStore.getProfileID(
							profileStore.getProfilebyname(enterProfileSpinner.getSelectedItem().toString())));
					location.setProfileExitId(-1);
				} else {
					Toast.makeText(LocationMap.this, "Must select at least on transition type", Toast.LENGTH_SHORT).show();
					return;
				}
				
				geoBuilder.setTransitionTypes(transitionType);
				
				List<Geofence> geofences = new ArrayList<Geofence>();
				geofences.add(geoBuilder.build());
				
				geofenceRequester.addGeofences(geofences);
				
				Iterator<Map.Entry<Marker, Circle>> it = markerCircleMap.entrySet().iterator();
				List<Marker> toRemove = new ArrayList<Marker>();
				while (it.hasNext()) {
					Map.Entry<Marker, Circle> pairs = it.next();
					if (pairs.getKey().getTitle().equals(location.getName())) {
						Marker marker = pairs.getKey();
						Circle circle = pairs.getValue();
						marker.remove();
						circle.remove();
						toRemove.add(marker);
					}
				}
				for (Marker marker : toRemove) {
					markerCircleMap.remove(marker);
				}
				markerCircleMap.put(
						map.addMarker(createMarkerOptions(new LatLng(location.getLatitude(), location.getLongitude()), location.getName(), snippetMaker(location))), 
						map.addCircle(createCircleOptions(new LatLng(location.getLatitude(), location.getLongitude()), location.getRadius())));
				
				locationStore.replaceLocation(location);
				
				Toast.makeText(LocationMap.this, "Location " + location.getName() + " Updated Successfully", Toast.LENGTH_SHORT).show();
			}
		});
		builder.setNeutralButton(R.string.delete_button, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				List<String> geofenceIdsToDelete = new ArrayList<String>();
				geofenceIdsToDelete.add(marker.getTitle());
				geofenceRemover.removeGeofencesById(geofenceIdsToDelete);
				
				locationStore.removeLocation(locationStore.getLocation(marker.getTitle()));
				
				markerCircleMap.get(marker).remove();
				marker.remove();
				
				Intent intent = new Intent(LocationMap.this, ProfileChangedReceiver.class);
				sendBroadcast(intent);
				
				Toast.makeText(LocationMap.this, "Location " + marker.getTitle() + " Deleted Successfully", Toast.LENGTH_SHORT).show();
			}
		});
		builder.setNegativeButton(R.string.negative_button, new OnClickListener() {
			
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
			}
		});
		
		List<Profile> profiles = profileStore.getProfiles();
		final List<String> profileNames = new ArrayList<String>();
		for (int i=0; i<profiles.size(); i++) {
			profileNames.add(profiles.get(i).getName());
		}
		enterProfileSpinner = (Spinner) view.findViewById(R.id.enter_profile);
		ArrayAdapter<String> adapter = 
				new ArrayAdapter<String>(LocationMap.this, android.R.layout.simple_spinner_item, profileNames);
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		enterProfileSpinner.setAdapter(adapter);
		
		exitProfileSpinner = (Spinner) view.findViewById(R.id.exit_profile);
		exitProfileSpinner.setAdapter(adapter);
		
		geofenceTitle.setText(location.getName());
		geofenceTitle.setEnabled(false);
		
		if (location.getProfileEnterId() != -1) {
			enterProfileSpinner.setSelection(profileNames.indexOf(profileStore.getProfilebyID(location.getProfileEnterId()).getName()));
			enterCheckBox.setChecked(true);
		} else {
			enterProfileSpinner.setEnabled(false);
			enterCheckBox.setChecked(false);
		}
		if (location.getProfileExitId() != -1) {
			exitProfileSpinner.setSelection(profileNames.indexOf(profileStore.getProfilebyID(location.getProfileExitId()).getName()));
			exitCheckBox.setChecked(true);
		} else {
			exitProfileSpinner.setEnabled(false);
			exitCheckBox.setChecked(false);
		}
		
		int selection = -1;
		switch ((int) location.getRadius()) {
		case (int) RADIUS_SMALL:
			selection = 0;
			break;

		case (int) RADIUS_MEDIUM:
			selection = 1;
			break;
		
		case (int) RADIUS_LARGE:
			selection = 2;
			break;
		}
		radiusSpinner.setSelection(selection);
		
		enterCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
				enterProfileSpinner.setEnabled(isChecked);
			}
			
		});
		
		exitCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
			
			@Override
			public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
				exitProfileSpinner.setEnabled(isChecked);
			}
		});
		
		builder.show();
	}

	private OnMapLongClickListener longClickListener() {
		return new OnMapLongClickListener() {
			
			@Override
			public void onMapLongClick(final LatLng point) {
				location = new Location();
				
				AlertDialog.Builder builder = new AlertDialog.Builder(LocationMap.this);
				LayoutInflater inflator = (LayoutInflater) getLayoutInflater();
				View view = inflator.inflate(R.layout.location_add_dialog, null);
				radiusSpinner = (Spinner) view.findViewById(R.id.radius_spinner);
				enterProfileSpinner = (Spinner) view.findViewById(R.id.enter_profile);
				exitProfileSpinner = (Spinner) view.findViewById(R.id.exit_profile);
				geofenceTitle = (EditText) view.findViewById(R.id.location_title);
				enterCheckBox = (CheckBox) view.findViewById(R.id.enter_profile_check);
				exitCheckBox = (CheckBox) view.findViewById(R.id.exit_profile_check);
				builder.setView(view);
				builder.setTitle(R.string.add_dialog_title);
				builder.setPositiveButton(R.string.positive_button, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if (enterProfileSpinner.getSelectedItem().toString().equals(MainActivity.NO_PROFILES)) {
							Intent intent = new Intent(LocationMap.this, AddProfile.class);
							intent.putExtra(AddProfile.mEXTRA_SSID, "no ssid");
							startActivity(intent);
							return;
						}
						
						location.setLatitude(point.latitude);
						location.setLongitude(point.longitude);
						Geofence.Builder geoBuilder = new Geofence.Builder();
						
						String radiusText = radiusSpinner.getSelectedItem().toString();
						float radius = 0;
						if (radiusText.equals("Small"))
							radius = RADIUS_SMALL;
						else if (radiusText.equals("Medium"))
							radius = RADIUS_MEDIUM;
						else if (radiusText.equals("Large"))
							radius = RADIUS_LARGE;
						
						location.setRadius(radius);
						
						geoBuilder.setCircularRegion(point.latitude, point.longitude, radius);
						geoBuilder.setExpirationDuration(geofenceExpiration);
						geoBuilder.setNotificationResponsiveness(geofenceResponsiveness);
						
						String title = geofenceTitle.getText().toString();
						if (title.isEmpty() || title == null) {
							Toast.makeText(LocationMap.this, "Title cannot be Empty", Toast.LENGTH_SHORT).show();
							return;
						}
						
						Log.i("LocationName", title);
						location.setName(title);
						
						geoBuilder.setRequestId(title);
						
						int transitionType = 0;
						if (enterCheckBox.isChecked() && exitCheckBox.isChecked()) {
							transitionType = Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT;
							location.setProfileEnterId((int) profileStore.getProfileID(
									profileStore.getProfilebyname(enterProfileSpinner.getSelectedItem().toString())));
							location.setProfileExitId((int) profileStore.getProfileID(
									profileStore.getProfilebyname(exitProfileSpinner.getSelectedItem().toString())));
						} else if (exitCheckBox.isChecked() && !enterCheckBox.isChecked()) {
							transitionType = Geofence.GEOFENCE_TRANSITION_EXIT;
							location.setProfileExitId((int) profileStore.getProfileID(
									profileStore.getProfilebyname(exitProfileSpinner.getSelectedItem().toString())));
							location.setProfileEnterId(-1);
						} else if (enterCheckBox.isChecked() && !exitCheckBox.isChecked()) {
							transitionType = Geofence.GEOFENCE_TRANSITION_ENTER;
							location.setProfileEnterId((int) profileStore.getProfileID(
									profileStore.getProfilebyname(enterProfileSpinner.getSelectedItem().toString())));
							location.setProfileExitId(-1);
						} else {
							Toast.makeText(LocationMap.this, "Must select at least on transition type", Toast.LENGTH_SHORT).show();
							return;
						}
						
						geoBuilder.setTransitionTypes(transitionType);
						
						if (locationStore.locationExists(title)) {
							Toast.makeText(LocationMap.this, "Geofence with Name: " + title + " already exists", Toast.LENGTH_SHORT).show();
							return;
						}
						
						List<Geofence> geofences = new ArrayList<Geofence>();
						geofences.add(geoBuilder.build());
						
						geofenceRequester.addGeofences(geofences);
						
						markerCircleMap.put(
								map.addMarker(createMarkerOptions(new LatLng(location.getLatitude(), location.getLongitude()), location.getName(), snippetMaker(location))), 
								map.addCircle(createCircleOptions(new LatLng(location.getLatitude(), location.getLongitude()), location.getRadius())));
						locationStore.addLocation(location);
						
						Toast.makeText(LocationMap.this, "Location " + location.getName() + " Created Successfully", Toast.LENGTH_SHORT).show();
					}
				});
				builder.setNegativeButton(R.string.negative_button, new OnClickListener() {
					
					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();
					}
				});
				List<Profile> profiles = profileStore.getProfiles();
				final List<String> profileNames = new ArrayList<String>();
				for (int i=0; i<profiles.size(); i++) {
					profileNames.add(profiles.get(i).getName());
				}
				if (profileNames.isEmpty())
					profileNames.add(MainActivity.NO_PROFILES);
				enterProfileSpinner = (Spinner) view.findViewById(R.id.enter_profile);
				ArrayAdapter<String> adapter = 
						new ArrayAdapter<String>(LocationMap.this, android.R.layout.simple_spinner_item, profileNames);
				adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
				enterProfileSpinner.setAdapter(adapter);
				
				exitProfileSpinner = (Spinner) view.findViewById(R.id.exit_profile);
				exitProfileSpinner.setAdapter(adapter);
				exitProfileSpinner.setEnabled(false);
				
				enterCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
						enterProfileSpinner.setEnabled(isChecked);
					}
					
				});
				
				exitCheckBox.setOnCheckedChangeListener(new OnCheckedChangeListener() {
					
					@Override
					public void onCheckedChanged(CompoundButton arg0, boolean isChecked) {
						exitProfileSpinner.setEnabled(isChecked);
					}
				});
				
				builder.show();
			}
		};
	}
	
	@Override
	public void onMarkerDragStart(Marker marker) {
		Circle circle = markerCircleMap.get(marker);
		circle.remove();
		markerCircleMap.remove(marker);
	}
	
	@Override
	public void onMarkerDrag(Marker marker) {
	}
	
	@Override
	public void onMarkerDragEnd(Marker marker) {
		Location location = locationStore.getLocation(marker.getTitle());
		location.setLatitude(marker.getPosition().latitude);
		location.setLongitude(marker.getPosition().longitude);

		markerCircleMap.put(marker, map.addCircle(createCircleOptions(marker.getPosition(), location.getRadius())));
		
		Geofence.Builder geobuilder = new Geofence.Builder();
		geobuilder.setCircularRegion(location.getLatitude(), location.getLongitude(), location.getRadius());
		geobuilder.setExpirationDuration(geofenceExpiration);
		geobuilder.setNotificationResponsiveness(geofenceResponsiveness);
		geobuilder.setRequestId(location.getName());
		if ((location.getProfileEnterId() != -1) && (location.getProfileExitId() != -1))
			geobuilder.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER | Geofence.GEOFENCE_TRANSITION_EXIT);
		else if (location.getProfileEnterId() != -1)
			geobuilder.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_ENTER);
		else if (location.getProfileExitId() != -1)
			geobuilder.setTransitionTypes(Geofence.GEOFENCE_TRANSITION_EXIT);
		
		List<Geofence> geofences = new ArrayList<Geofence>();
		geofences.add(geobuilder.build());
		
		geofenceRequester.addGeofences(geofences);
		
		locationStore.replaceLocation(location);
		
		Toast.makeText(LocationMap.this, "Location " + location.getName() + " Updated Successfully", Toast.LENGTH_SHORT).show();
	}
	
	@Override
	protected void onPause() {
		locationStore.close();
		profileStore.close();
		super.onPause();
	}
	
	@Override
	protected void onResume() {
		locationStore.open();
		profileStore.open();
		
		displayFences(locationStore.getLocations());

		SharedPreferences prefs = getSharedPreferences(MainActivity.PrefsName, 0);
		Editor editor = prefs.edit();
		editor.putInt(MainActivity.current_page_key, 2);
		editor.commit();
		
		super.onResume();
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.map, menu);
		return true;
	}
	
	@Override
	public boolean onMenuItemSelected(int featureId, MenuItem item) {
		switch (item.getItemId()) {
		case R.id.goto_item:
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setTitle(R.string.goto_dialog_title);
			final List<Location> locations = locationStore.getLocations();
			if (locations.isEmpty())
				locations.add(new Location(){
					@Override
					public String getName() {
						return NO_LOCATIONS;
					}
					
					@Override
					public int getProfileEnterId() {
						return -1;
					}
					
					@Override
					public int getProfileExitId() {
						return -1;
					}
				});
			LocationArrayAdapter adapter = new LocationArrayAdapter(this, locations);
			builder.setSingleChoiceItems(adapter, 0, new OnClickListener() {
				
				@Override
				public void onClick(DialogInterface dialog, int which) {
					if (locations.get(which).getName().equals(NO_LOCATIONS)) {
						return;
					}
					
					dialog.dismiss();
					Location location = locations.get(which);
					map.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), DEFAULT_ZOOM));
				}
			}).show();
			break;
			
		}
		return super.onMenuItemSelected(featureId, item);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		}
		return super.onOptionsItemSelected(item);
	}
	
	private void displayFences(List<Location> locations) {
		for (Location location : locations) {
			markerCircleMap.put(
					map.addMarker(createMarkerOptions(new LatLng(location.getLatitude(), location.getLongitude()), location.getName(), snippetMaker(location))), 
					map.addCircle(createCircleOptions(new LatLng(location.getLatitude(), location.getLongitude()), location.getRadius())));
		}
	}
	
	private String snippetMaker(Location location) {
		String snippet = "";
		if (location.getProfileEnterId() != -1)
			snippet += "On Enter: " + profileStore.getProfilebyID(location.getProfileEnterId()).getName();
		else
			snippet += "On Enter: NONE";
		if (location.getProfileExitId() != -1)
			snippet += ",On Exit: " + profileStore.getProfilebyID(location.getProfileExitId()).getName();
		else
			snippet += ",On Exit: NONE";
		return snippet;
	}
	
	private MarkerOptions createMarkerOptions(LatLng point, String title, String content) {
		return new MarkerOptions()
				.position(point)
				.title(title)
				.snippet(content)
				.draggable(true);
	}
	
	private CircleOptions createCircleOptions(LatLng point, double radius) {
		return new CircleOptions()
				.center(point)
				.fillColor(fillColor)
				.radius(radius)
				.strokeColor(strokeColor)
				.strokeWidth(strokeWidth);
	}

}
