package com.WifiProfiles;

import java.util.ArrayList;
import java.util.List;

import android.app.AlarmManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ListView;
import android.widget.Toast;

import com.WifiProfiles.Adapters.ConfiguredOptionsArrayAdapter;
import com.WifiProfiles.Adapters.EventArrayAdapter;
import com.WifiProfiles.Adapters.LocationArrayAdapter;
import com.WifiProfiles.Domain.ConfiguredRouter;
import com.WifiProfiles.Domain.Event;
import com.WifiProfiles.Domain.Location;
import com.WifiProfiles.Domain.Profile;
import com.WifiProfiles.Receivers.AlarmReceiver;
import com.WifiProfiles.Receivers.ProfileChangedReceiver;
import com.WifiProfiles.Stores.EventStore;
import com.WifiProfiles.Stores.GeofenceRemover;
import com.WifiProfiles.Stores.LocationStore;
import com.WifiProfiles.Stores.ProfileStore;

/**
 * A dummy fragment representing a section of the app, but that simply
 * displays dummy text.
 */
public class MainFragment extends Fragment {

		private int pageNumber;
		
		private ConfiguredOptionsArrayAdapter adapter;
		private ConfiguredOptionsArrayAdapter emptyAdapter;
		private EventArrayAdapter eventAdapter;
		private LocationArrayAdapter locationAdapter;
		
		/**
		 * The fragment argument representing the section number for this
		 * fragment.
		 */
		public static final String ARG_SECTION_NUMBER = "section_number";

		public static MainFragment create(int pageNumber) {
			Log.d("Create Fragment", "Creating new fragment for page: " + pageNumber);
			MainFragment fragment = new MainFragment();
			Bundle args = new Bundle();
			args.putInt(ARG_SECTION_NUMBER, pageNumber);
			fragment.setArguments(args);
			return fragment;
		}
		
		public MainFragment() {
		}
		
		@Override
		public void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			pageNumber = getArguments().getInt(ARG_SECTION_NUMBER);
		}
		
		public int getPageNumber() {
			return pageNumber;
		}

		@Override
		public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
			View rootView = inflater.inflate(com.sechristfamily.WifiProfiles.R.layout.fragment_layout, container, false);
			ListView routerList = (ListView) rootView.findViewById(com.sechristfamily.WifiProfiles.R.id.configured_routers);
			
			ProfileStore profileStore = new ProfileStore(getActivity());
			profileStore.open();
			EventStore eventStore = new EventStore(getActivity());
			eventStore.open();
			LocationStore locationStore = new LocationStore(getActivity());
			locationStore.open();
			
			if (pageNumber == 0) {
				List<ConfiguredRouter> routers = getConfiguredRouters(profileStore);
				if (routers.isEmpty()) {
					List<ConfiguredRouter> msgs = new ArrayList<ConfiguredRouter>();
					ConfiguredRouter msg = new ConfiguredRouter();
					msg.setSsid("Add a Router by selecting the +");
					Profile tempProfile = new Profile();
					tempProfile.setName("");
					msg.setProfile(tempProfile);
					msgs.add(msg);
					adapter = new ConfiguredOptionsArrayAdapter(getActivity(), msgs);
					routerList.setAdapter(adapter);
				} else {
					adapter = new ConfiguredOptionsArrayAdapter(getActivity(), routers);
					routerList.setAdapter(adapter);
					routerList.setOnItemLongClickListener(deleteListener);
				}
			} else if (pageNumber == 1) {
				List<Event> events = eventStore.getEvents();
				if (events.isEmpty()) {
					List<ConfiguredRouter> msgs = new ArrayList<ConfiguredRouter>();
					ConfiguredRouter msg = new ConfiguredRouter();
					msg.setSsid("Add an Event by selecting the +");
					Profile tempProfile = new Profile();
					tempProfile.setName("");
					msg.setProfile(tempProfile);
					msgs.add(msg);
					emptyAdapter = new ConfiguredOptionsArrayAdapter(getActivity(), msgs);
					routerList.setAdapter(emptyAdapter);
				} else {
					eventAdapter = new EventArrayAdapter(getActivity(), events);
					routerList.setAdapter(eventAdapter);
					routerList.setOnItemLongClickListener(eventDelete);
				}
			} else if (pageNumber == 2) {
				List<Location> locations = locationStore.getLocations();
				if (locations.isEmpty()) {
					List<ConfiguredRouter> msgs = new ArrayList<ConfiguredRouter>();
					ConfiguredRouter msg = new ConfiguredRouter();
					msg.setSsid("Add a Location by selecting the +");
					Profile tempProfile = new Profile();
					tempProfile.setName("");
					msg.setProfile(tempProfile);
					msgs.add(msg);
					emptyAdapter = new ConfiguredOptionsArrayAdapter(getActivity(), msgs);
					routerList.setAdapter(emptyAdapter);
				} else {
					locationAdapter = new LocationArrayAdapter(getActivity(), locations);
					routerList.setAdapter(locationAdapter);
					routerList.setOnItemLongClickListener(locationDelete());
				}
			}
			
			profileStore.close();
			eventStore.close();
			locationStore.close();
			
			return rootView;
		}

		private List<ConfiguredRouter> getConfiguredRouters(ProfileStore profileStore) {
			List<ConfiguredRouter> routers = new ArrayList<ConfiguredRouter>();
			List<String> ssids = profileStore.getSSIDs();
			for(String ssid : ssids) {
				Profile profile = profileStore.getProfilebySSID(ssid);
				ConfiguredRouter router = new ConfiguredRouter();
				router.setSsid(ssid);
				router.setProfile(profile);
				routers.add(router);
			}
			return routers;
		}
		
		private OnItemLongClickListener deleteListener = new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int position, long arg3) {
				if((adapter.getItem(position).getSsid() == "Add a Router by selecting the +") || (adapter.getItem(position).getSsid() == "Add a Router by selecting 'Add'\nfrom the settings menu"))
					return false;
				final int i = position;
				AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
				alertBuilder.setMessage("Would you like to Delete " + adapter.getItem(position).getSsid() + "?")
					.setPositiveButton("Yes", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							dialog.dismiss();
							ConfiguredRouter router = adapter.getItem(i);
							ProfileStore profileStore = new ProfileStore(getActivity());
							profileStore.open();
							profileStore.removeSSID(router.getSsid());
							profileStore.close();
							Intent intent = new Intent(getActivity(), ProfileChangedReceiver.class);
							getActivity().sendBroadcast(intent);
							Toast.makeText(getActivity(), "Deleted " + router.getSsid(), Toast.LENGTH_SHORT).show();
							
							updateNotis();
							
							adapter.remove(router);
						}
					})
					.setNegativeButton("No", null);
				AlertDialog dialog = alertBuilder.create();
				dialog.show();
				return true;
			}
			
		};
		
		private OnItemLongClickListener eventDelete = new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					final int position, long arg3) {
				AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
				alertBuilder.setMessage("Would you like to Delete this Event?")
					.setPositiveButton("Yes", new OnClickListener() {
						
						@Override
						public void onClick(DialogInterface dialog, int which) {
							Event event = eventAdapter.getItem(position);
							EventStore eventStore = new EventStore(getActivity());
							eventStore.open();
							eventStore.removeEvent(event.getAlarmID());
							eventStore.close();
							Intent intent = new Intent(getActivity(), AlarmReceiver.class);
							PendingIntent pIntent = PendingIntent.getBroadcast(getActivity(), event.getAlarmID(), 
									intent, PendingIntent.FLAG_CANCEL_CURRENT);
							AlarmManager alarmman = (AlarmManager)getActivity().getSystemService(Context.ALARM_SERVICE);
							alarmman.cancel(pIntent);
							Toast.makeText(getActivity(), "Deleted Event", Toast.LENGTH_SHORT).show();
							
							dialog.dismiss();
							
							updateNotis();
							
							eventAdapter.remove(event);
						}
					})
					.setNegativeButton("No", null);
				AlertDialog dialog = alertBuilder.create();
				dialog.show();
				return true;
			}
		};
		
		private OnItemLongClickListener locationDelete() {
			return new OnItemLongClickListener() {

				@Override
				public boolean onItemLongClick(AdapterView<?> arg0,
						View arg1, final int position, long arg3) {
					AlertDialog.Builder alertBuilder = new AlertDialog.Builder(getActivity());
					alertBuilder.setMessage("Would you like to Delete this Geofence?")
						.setPositiveButton("Yes", new OnClickListener() {
							
							@Override
							public void onClick(DialogInterface dialog, int which) {
								Location location = locationAdapter.getItem(position);
								LocationStore locationStore = new LocationStore(getActivity());
								locationStore.open();
								locationStore.removeLocation(location);
								locationStore.close();
								
								GeofenceRemover remover = new GeofenceRemover(getActivity());
								List<String> geofenceIds = new ArrayList<String>();
								geofenceIds.add(location.getName());
								remover.removeGeofencesById(geofenceIds);
								
								Toast.makeText(getActivity(), "Deleted Location", Toast.LENGTH_SHORT).show();
								
								dialog.dismiss();
								
								updateNotis();
								
								locationAdapter.remove(location);
							}
						})
						.setNegativeButton("No", null);
					alertBuilder.show();
					return true;
				}
			};
		}
		
		private void updateNotis() {
			Intent intent = new Intent(getActivity(), ProfileChangedReceiver.class);
			getActivity().sendBroadcast(intent);
		}
		
}
