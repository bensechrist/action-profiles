package com.WifiProfiles.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.WifiProfiles.Domain.Profile;

public class ProfileArrayAdapter extends ArrayAdapter<Profile> {

	private Context context;
	private Profile[] profiles;

	public ProfileArrayAdapter(Context context, Profile[] values) {
		super(context, com.sechristfamily.WifiProfiles.R.layout.profile_row_layout, values);
		this.context = context;
		this.profiles = values;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Profile profile = profiles[position];
		
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(com.sechristfamily.WifiProfiles.R.layout.profile_row_layout, parent, false);
		TextView profileName = (TextView) rowView.findViewById(com.sechristfamily.WifiProfiles.R.id.profile_name);
		profileName.setText(profile.getName());
		profileName.setTextSize(25);
		return rowView;
	}
}
