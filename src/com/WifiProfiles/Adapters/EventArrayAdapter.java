package com.WifiProfiles.Adapters;

import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.WifiProfiles.Domain.Event;
import com.WifiProfiles.Stores.ProfileStore;

public class EventArrayAdapter extends ArrayAdapter<Event>{

	private Context context;
	public EventArrayAdapter(Context context, List<Event> events) {
		super(context, com.sechristfamily.WifiProfiles.R.layout.event_row_layout, events);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Event event = getItem(position);
		ProfileStore profileStore = new ProfileStore(getContext());
		profileStore.open();
		
		Calendar cal = Calendar.getInstance();
		cal.setTimeInMillis(event.getEventTimeInMills());
		
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(com.sechristfamily.WifiProfiles.R.layout.event_row_layout, parent, false);
		com.WifiProfiles.Fonts.CustomTextViewNormal eventName = (com.WifiProfiles.Fonts.CustomTextViewNormal) 
				rowView.findViewById(com.sechristfamily.WifiProfiles.R.id.event_date_time);
		com.WifiProfiles.Fonts.CustomTextViewNormal profileName = (com.WifiProfiles.Fonts.CustomTextViewNormal) 
				rowView.findViewById(com.sechristfamily.WifiProfiles.R.id.event_profile_name);
		
		int hour;
		if(cal.get(Calendar.HOUR) == 0)
			hour = 12;
		else
			hour = cal.get(Calendar.HOUR);
		String am_pm = "AM";
		if(cal.get(Calendar.AM_PM) == 1)
			am_pm = "PM";
		String min = String.valueOf(cal.get(Calendar.MINUTE));
		if(min.length() == 1)
			min = "0" + String.valueOf(cal.get(Calendar.MINUTE));

		String day_of_week = "Undefined";
		switch (cal.get(Calendar.DAY_OF_WEEK)) {
		case 1:
			day_of_week = "Sunday";
			break;

		case 2:
			day_of_week = "Monday";
			break;
			
		case 3:
			day_of_week = "Tuesday";
			break;
			
		case 4:
			day_of_week = "Wednesday";
			break;
			
		case 5:
			day_of_week = "Thursday";
			break;
			
		case 6:
			day_of_week = "Friday";
			break;
			
		case 7:
			day_of_week = "Saturday";
			break;
		}
		
		if(event.getRecurring() == null)
			eventName.setText((cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.YEAR) + " " + hour + ":" + min + " " + am_pm);
		else if((event.getRecurring().equals("Every Day")) || (event.getRecurring().equals("Every Other Day")))
			eventName.setText(event.getRecurring() + " at " + hour + ":" + min + " " + am_pm + "\nStarting " + (cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.YEAR));
		else if(event.getRecurring().equals("Every Week"))
			eventName.setText("Every " + day_of_week + " at " + hour + ":" + min + " " + am_pm + "\nStarting " + (cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.YEAR));
		else if(event.getRecurring().equals("Every Other Week"))
			eventName.setText("Every other " + day_of_week + " at " + hour + ":" + min + " " + am_pm + "\nStarting " + (cal.get(Calendar.MONTH)+1) + "/" + cal.get(Calendar.DAY_OF_MONTH) + "/" + cal.get(Calendar.YEAR));
		profileName.setText(profileStore.getProfilebyID(event.getIdToProfile()).getName());
		eventName.setTextSize(20);
		profileName.setTextSize(20);
		profileStore.close();
		return rowView;
	}
}
