package com.WifiProfiles.Adapters;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.WifiProfiles.MainActivity;

public class CustomTextArrayAdapter extends ArrayAdapter<String> {

	public CustomTextArrayAdapter(Context context, int resource,
			int textViewResourceId, String[] objects) {
		super(context, resource, textViewResourceId, objects);
	}	
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		View view = super.getView(position, convertView, parent);
		TextView tv = (TextView) view.findViewById(android.R.id.text1);
		tv.setTypeface(MainActivity.customFont);
		tv.setTextSize(25);
		return tv;
	}

}
