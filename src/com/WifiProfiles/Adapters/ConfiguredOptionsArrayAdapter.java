package com.WifiProfiles.Adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.WifiProfiles.Domain.ConfiguredRouter;

public class ConfiguredOptionsArrayAdapter extends ArrayAdapter<ConfiguredRouter> {

	private Context context;
	public ConfiguredOptionsArrayAdapter(Context context, List<ConfiguredRouter> ssid) {
		super(context, com.sechristfamily.WifiProfiles.R.layout.configured_row_layout, ssid);
		this.context = context;
	}

	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		ConfiguredRouter profile = getItem(position);
		
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflater.inflate(com.sechristfamily.WifiProfiles.R.layout.configured_row_layout, parent, false);
		com.WifiProfiles.Fonts.CustomTextViewNormal ssidName = (com.WifiProfiles.Fonts.CustomTextViewNormal) 
				rowView.findViewById(com.sechristfamily.WifiProfiles.R.id.ssid_name);
		com.WifiProfiles.Fonts.CustomTextViewNormal profileName = (com.WifiProfiles.Fonts.CustomTextViewNormal) 
				rowView.findViewById(com.sechristfamily.WifiProfiles.R.id.profile_name);
		ssidName.setText(profile.getSsid());
		profileName.setText(profile.getProfile().getName());
		ssidName.setTextSize(25);
		profileName.setTextSize(25);
		return rowView;
	}
}
