package com.WifiProfiles.Adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap.InfoWindowAdapter;
import com.google.android.gms.maps.model.Marker;
import com.sechristfamily.WifiProfiles.R;

public class MapPopupAdapter implements InfoWindowAdapter {
	
	private Context context;
	
	public MapPopupAdapter(Context context) {
		this.context = context;
	}
	
	@Override
	public View getInfoContents(Marker marker) {
		LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View popup = inflater.inflate(R.layout.map_popup, null);
		
		TextView title = (TextView) popup.findViewById(R.id.popup_title);
		TextView enter = (TextView) popup.findViewById(R.id.popup_enter_profile);
		TextView exit = (TextView) popup.findViewById(R.id.popup_exit_profile);
		
		title.setText(marker.getTitle());
		enter.setText(marker.getSnippet().split(",")[0]);
		exit.setText(marker.getSnippet().split(",")[1]);
		
		return popup;
	}

	@Override
	public View getInfoWindow(Marker marker) {
		// TODO Auto-generated method stub
		return null;
	}

}
