package com.WifiProfiles.Adapters;

import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;

import com.WifiProfiles.Domain.Location;
import com.WifiProfiles.Stores.ProfileStore;

public class LocationArrayAdapter extends ArrayAdapter<Location> {

	private Context context;
	public LocationArrayAdapter(Context context, List<Location> locations) {
		super(context, com.sechristfamily.WifiProfiles.R.layout.event_row_layout, locations);
		this.context = context;
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		Location location = getItem(position);
		ProfileStore profileStore = new ProfileStore(context);
		profileStore.open();
		
		LayoutInflater inflator = (LayoutInflater) 
				context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		View rowView = inflator.inflate(
				com.sechristfamily.WifiProfiles.R.layout.location_row_layout, parent, false);
		com.WifiProfiles.Fonts.CustomTextViewNormal locationName = 
				(com.WifiProfiles.Fonts.CustomTextViewNormal) rowView.findViewById(com.sechristfamily.WifiProfiles.R.id.location_title);
		com.WifiProfiles.Fonts.CustomTextViewNormal profileName = 
				(com.WifiProfiles.Fonts.CustomTextViewNormal) rowView.findViewById(com.sechristfamily.WifiProfiles.R.id.location_profile_name);
		
		locationName.setText(location.getName());
		locationName.setTextSize(25);
		int enterId = location.getProfileEnterId();
		int exitId = location.getProfileExitId();
		if ((enterId != -1) && (exitId != -1)) {
			profileName.setText("Enter: " + profileStore.getProfilebyID(enterId).getName()
					+ "\nExit: " + profileStore.getProfilebyID(exitId).getName());
		} else if (enterId != -1) {
			profileName.setText("Enter: " + profileStore.getProfilebyID(enterId).getName());
		} else if (exitId != -1) {
			profileName.setText("Exit: " + profileStore.getProfilebyID(exitId).getName());
		} else if ((enterId == -1) && (exitId == -1)) {
			((RelativeLayout)profileName.getParent()).removeView(profileName);
			profileStore.close();
			return rowView;
		}
		profileName.setTextSize(20);
		profileStore.close();
		return rowView;
	}
	
}
