package com.WifiProfiles.Receivers;

import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.util.Log;
import com.WifiProfiles.MainActivity;
import com.WifiProfiles.Domain.Event;
import com.WifiProfiles.Domain.Profile;
import com.WifiProfiles.Services.NotificationService;
import com.WifiProfiles.Stores.EventStore;
import com.WifiProfiles.Stores.ProfileStore;

public class AlarmReceiver extends BroadcastReceiver {
	
	private AudioManager am;
	
	public static String alarmNotiExtra = "NotificationFromAlarm";
	
	@SuppressLint("NewApi")
	@Override
	public void onReceive(Context context, Intent intent) {
		
		Log.i("AlarmReceiver", "Received Alarm");
		
		SharedPreferences prefs = context.getSharedPreferences(MainActivity.PrefsName, 0);
		
		am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		
		EventStore eventStore = new EventStore(context);
		eventStore.open();
		ProfileStore profileStore = new ProfileStore(context);
		profileStore.open();
		
		String name = intent.getStringExtra("profilename");
		long timeInMillis = intent.getLongExtra("eventTimeInMillis", 0);
		if ((timeInMillis == 0)) {
			eventStore.close();
			profileStore.close();
			return;
		}
		Profile tempProfile = profileStore.getProfilebyname(name);
		if (tempProfile != null){
			if (!prefs.getBoolean(MainActivity.service_key, true)) {
				if (!intent.getBooleanExtra("reoccur", false))
					eventStore.removeEvent(eventStore.getAlarmID(intent.getLongExtra("eventTimeInMillis", 0)));
				eventStore.close();
				profileStore.close();
				return;
			}
			set_profile(tempProfile);
			int alarmId = eventStore.getAlarmID(timeInMillis);
			Event event = eventStore.getEvent(alarmId);
			if (alarmId == -1) {
				eventStore.close();
				profileStore.close();
				return;
			}
			Intent onClickIntent = new Intent(context, MainActivity.class);
			onClickIntent.putExtra(NotificationService.notificationExtra, alarmNotiExtra);
			onClickIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
			PendingIntent pintent = PendingIntent.getActivity(context, 1, onClickIntent, PendingIntent.FLAG_CANCEL_CURRENT);
			NotificationService.set_notification(context, "Event", tempProfile.getName(), "Wifi Profiles Event", 
					prefs.getString(WifiStateChangeReceiver.lastRouterKey, ""), pintent);
			Log.i("recurring", String.valueOf(intent.getBooleanExtra("reoccur", false)));
			if(!intent.getBooleanExtra("reoccur", false))
				eventStore.removeEvent(alarmId);
			else if (android.os.Build.VERSION_CODES.KITKAT < android.os.Build.VERSION.SDK_INT) {
				Intent temp = new Intent(context, AlarmReceiver.class);
				temp.putExtra("profilename", tempProfile.getName());
				temp.putExtra("reoccur", intent.getBooleanExtra("reoccur", false));
				temp.putExtra("eventTimeInMillis", timeInMillis);
				PendingIntent pintent_setProfile = PendingIntent.getBroadcast(context, alarmId, 
						temp, PendingIntent.FLAG_CANCEL_CURRENT);
				
				long repeatTime = 0;
				
				switch (event.getRecurring()) {
				case "Every Day":
					repeatTime = 86400000;
					break;
					
				case "Every Other Day":
					repeatTime = 86400000*2;
					break;
					
				case "Every Week":
					repeatTime = 604800000;
					break;
					
				case "Every Other Week":
					repeatTime = 604800000*2;
					break;
				}
				
				while (timeInMillis < System.currentTimeMillis()) {
					timeInMillis += repeatTime;
				}
				
				AlarmManager alarmMan = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
				alarmMan.setExact(AlarmManager.RTC_WAKEUP, timeInMillis, pintent_setProfile);
			}
		}
		
		eventStore.close();
		profileStore.close();
	}
	
	private void set_profile(Profile profile){
		if (profile.isSilent()) {
			Log.i("silent", "It's silent");
			am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		} else if (profile.isVibrate()) {
			am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
		} else {
			Log.i("silent", "No silent");
			int ring = profile.getRinger_vol();
			am.setStreamVolume(AudioManager.STREAM_RING, ring, 0);
			int notvol = profile.getNotification_vol();
			am.setStreamVolume(AudioManager.STREAM_NOTIFICATION, notvol, 0);
		}
		int mediavol = profile.getMedia_vol();
		if (HeadsetPlugReceiver.connectedHeadphones) {
			HeadsetPlugReceiver.lastmediavol = mediavol;
		} else {
			am.setStreamVolume(AudioManager.STREAM_MUSIC, mediavol, 0);
			am.setStreamVolume(AudioManager.STREAM_SYSTEM, mediavol, 0);
		}
	}
}
