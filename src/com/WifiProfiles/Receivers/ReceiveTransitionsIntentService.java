package com.WifiProfiles.Receivers;

import java.util.List;

import android.app.IntentService;
import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;

import com.WifiProfiles.LocationMap;
import com.WifiProfiles.MainActivity;
import com.WifiProfiles.Domain.Location;
import com.WifiProfiles.Domain.Profile;
import com.WifiProfiles.Services.NotificationService;
import com.WifiProfiles.Stores.LocationStore;
import com.WifiProfiles.Stores.ProfileStore;
import com.WifiProfiles.Utils.GeofenceUtils;
import com.WifiProfiles.Utils.LocationServiceErrorMessages;
import com.google.android.gms.location.Geofence;
import com.google.android.gms.location.LocationClient;
import com.sechristfamily.WifiProfiles.R;

/**
 * This class receives geofence transition events from Location Services, in the
 * form of an Intent containing the transition type and geofence id(s) that triggered
 * the event.
 */
public class ReceiveTransitionsIntentService extends IntentService {
	
	private SharedPreferences sp;
	
	public static String geofenceNotiExtra = "NotificationFromGeofence";
	
    /**
     * Sets an identifier for this class' background thread
     */
    public ReceiveTransitionsIntentService() {
        super("ReceiveTransitionsIntentService");
    }

    /**
     * Handles incoming intents
     * @param intent The Intent sent by Location Services. This Intent is provided
     * to Location Services (inside a PendingIntent) when you call addGeofences()
     */
    @Override
    protected void onHandleIntent(Intent intent) {
    	sp = getSharedPreferences(MainActivity.PrefsName, 0);
    	
    	if (!sp.getBoolean(MainActivity.service_key, true))
			return;

        // Create a local broadcast Intent
        Intent broadcastIntent = new Intent();

        // Give it the category for all intents sent by the Intent Service
        broadcastIntent.addCategory(GeofenceUtils.CATEGORY_LOCATION_SERVICES);

        // First check for errors
        if (LocationClient.hasError(intent)) {

            // Get the error code
            int errorCode = LocationClient.getErrorCode(intent);

            // Get the error message
            String errorMessage = LocationServiceErrorMessages.getErrorString(this, errorCode);

            // Log the error
            Log.e(GeofenceUtils.APPTAG,
                    getString(R.string.geofence_transition_error_detail, errorMessage)
            );

            // Set the action and error message for the broadcast intent
            broadcastIntent.setAction(GeofenceUtils.ACTION_GEOFENCE_ERROR)
                           .putExtra(GeofenceUtils.EXTRA_GEOFENCE_STATUS, errorMessage);

            // Broadcast the error *locally* to other components in this app
            LocalBroadcastManager.getInstance(this).sendBroadcast(broadcastIntent);

        // If there's no error, get the transition type and create a notification
        } else {

            // Get the type of transition (entry or exit)
            int transition = LocationClient.getGeofenceTransition(intent);

            // Test for geofence enter
            if (transition == Geofence.GEOFENCE_TRANSITION_ENTER) {
            	Log.d("Geofence Transition", getTransitionString(transition));

                // Post a notification
                List<Geofence> geofences = LocationClient.getTriggeringGeofences(intent);
                String[] geofenceIds = new String[geofences.size()];
                String[] profiles = new String[geofences.size()];
                for (int index = 0; index < geofences.size() ; index++) {
                    geofenceIds[index] = geofences.get(index).getRequestId();
                    profiles[index] = applyProfile(geofenceIds[index], transition);
                }
                String ids = TextUtils.join(GeofenceUtils.GEOFENCE_ID_DELIMITER,geofenceIds);
                String transitionType = getTransitionString(transition);
                String profileNames = TextUtils.join(", ", profiles);

                String title = getString(R.string.geofence_transition_notification_title, transitionType, ids);
                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                Intent mapIntent = new Intent(getApplicationContext(), LocationMap.class);
                mapIntent.putExtra(NotificationService.notificationExtra, geofenceNotiExtra);
                PendingIntent pintent = PendingIntent.getActivities(getApplicationContext(), 2, 
                		new Intent[] {mainIntent, mapIntent}, PendingIntent.FLAG_CANCEL_CURRENT);
                NotificationService.set_notification(getApplicationContext(), title, profileNames, title, 
                		sp.getString(WifiStateChangeReceiver.lastRouterKey, ""), pintent);

                // Log the transition type and a message
                Log.d(GeofenceUtils.APPTAG,
                        getString(
                                R.string.geofence_transition_notification_title,
                                transitionType,
                                ids));
                Log.d(GeofenceUtils.APPTAG,
                        getString(R.string.geofence_transition_notification_text,
                        		profileNames));

            // Test for geofence exit
            } else if (transition == Geofence.GEOFENCE_TRANSITION_EXIT) {
            	
            	// Post a notification
                List<Geofence> geofences = LocationClient.getTriggeringGeofences(intent);
                String[] geofenceIds = new String[geofences.size()];
                String[] profiles = new String[geofences.size()];
                for (int index = 0; index < geofences.size() ; index++) {
                    geofenceIds[index] = geofences.get(index).getRequestId();
                    profiles[index] = applyProfile(geofenceIds[index], transition);
                }
                String ids = TextUtils.join(GeofenceUtils.GEOFENCE_ID_DELIMITER,geofenceIds);
                String transitionType = getTransitionString(transition);
                String profileNames = TextUtils.join(", ", profiles);

                String title = getString(R.string.geofence_transition_notification_title, transitionType, ids);
                Intent mainIntent = new Intent(getApplicationContext(), MainActivity.class);
                mainIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
                Intent mapIntent = new Intent(getApplicationContext(), LocationMap.class);
                mapIntent.putExtra(NotificationService.notificationExtra, geofenceNotiExtra);
                PendingIntent pintent = PendingIntent.getActivities(getApplicationContext(), 2, 
                		new Intent[] {mainIntent, mapIntent}, PendingIntent.FLAG_CANCEL_CURRENT);
                NotificationService.set_notification(getApplicationContext(), title, profileNames, title, 
                		sp.getString(WifiStateChangeReceiver.lastRouterKey, ""), pintent);

                // Log the transition type and a message
                Log.d(GeofenceUtils.APPTAG,
                        getString(
                                R.string.geofence_transition_notification_title,
                                transitionType,
                                ids));
                Log.d(GeofenceUtils.APPTAG,
                        getString(R.string.geofence_transition_notification_text,
                        		profileNames));
            	
            // An invalid transition was reported
            } else {
                // Always log as an error
                Log.e(GeofenceUtils.APPTAG,
                        getString(R.string.geofence_transition_invalid_type, transition));
            }
        }
    }
    
    /**
     * Maps geofence transition types to their human-readable equivalents.
     * @param transitionType A transition type constant defined in Geofence
     * @return A String indicating the type of transition
     */
    private String getTransitionString(int transitionType) {
        switch (transitionType) {

            case Geofence.GEOFENCE_TRANSITION_ENTER:
                return getString(R.string.geofence_transition_entered);

            case Geofence.GEOFENCE_TRANSITION_EXIT:
                return getString(R.string.geofence_transition_exited);

            default:
                return getString(R.string.geofence_transition_unknown);
        }
    }
    
    private String applyProfile(String name, int transitionType) {
    	LocationStore locationStore = new LocationStore(getApplicationContext());
    	ProfileStore profileStore = new ProfileStore(getApplicationContext());
    	locationStore.open();
    	profileStore.open();
    	
    	Log.i("Apply Profile", "Getting location with name " + name);
    	Location location = locationStore.getLocation(name);
    	
    	Profile profile = new Profile();
    	switch (transitionType) {
		case Geofence.GEOFENCE_TRANSITION_ENTER:
			profile = profileStore.getProfilebyID(location.getProfileEnterId());
			break;

		case Geofence.GEOFENCE_TRANSITION_EXIT:
			profile = profileStore.getProfilebyID(location.getProfileExitId());
			break;
		}
    	
    	set_profile(profile);
    	
    	locationStore.close();
    	profileStore.close();
    	
    	return profile.getName();
    }
    
    private void set_profile(Profile profile){
    	AudioManager am = (AudioManager) getApplicationContext().getSystemService(AUDIO_SERVICE);
    	
		if (profile.isSilent()) {
			Log.i("silent", "It's silent");
			am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		} else if (profile.isVibrate()) {
			am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
		} else {
			Log.i("silent", "No silent");
			int ring = profile.getRinger_vol();
			am.setStreamVolume(AudioManager.STREAM_RING, ring, 0);
			int notvol = profile.getNotification_vol();
			am.setStreamVolume(AudioManager.STREAM_NOTIFICATION, notvol, 0);
		}
		int mediavol = profile.getMedia_vol();
		if (HeadsetPlugReceiver.connectedHeadphones) {
			HeadsetPlugReceiver.lastmediavol = mediavol;
		} else {
			am.setStreamVolume(AudioManager.STREAM_MUSIC, mediavol, 0);
			am.setStreamVolume(AudioManager.STREAM_SYSTEM, mediavol, 0);
		}
	}
}
