package com.WifiProfiles.Receivers;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.app.Notification.Builder;
import android.app.backup.BackupManager;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.media.AudioManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.WifiProfiles.MainActivity;
import com.WifiProfiles.Domain.Profile;
import com.WifiProfiles.Services.NotificationService;
import com.WifiProfiles.Stores.ProfileStore;

public class WifiStateChangeReceiver extends BroadcastReceiver {
	
	private SharedPreferences sp;
	private ProfileStore profileStore;
	private AudioManager am;
	private NotificationManager notman;
	private BackupManager backupMng;
	
	public static int mId = 001;
	public static int newssid_id = 002;
	public static String lastRouterKey = "LastRouter";
	public static String wifiNotiExtra = "NotificationFromWifi";
	
	@Override
	public void onReceive(Context context, Intent intent) {
		Log.i("receiver", "received it");
		
		sp = context.getSharedPreferences(MainActivity.PrefsName, 0);
		
		backupMng = new BackupManager(context);
		
		if (!sp.getBoolean(MainActivity.service_key, true))
			return;
		
		profileStore = new ProfileStore(context);
		profileStore.open();
		
		am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		
		NetworkInfo nInfo = intent.getParcelableExtra(WifiManager.EXTRA_NETWORK_INFO);
		
		notman = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		Editor editor = sp.edit();
		if (nInfo.isConnected() && (nInfo.getType() == ConnectivityManager.TYPE_WIFI)) {
			WifiManager wifiman = (WifiManager) context.getSystemService(Context.WIFI_SERVICE);
			WifiInfo winfo = wifiman.getConnectionInfo();
			String ssid = winfo.getSSID().replace("\"", "");
			if (ssid.isEmpty()) {
				notman.cancelAll();
				editor.remove(lastRouterKey);
				editor.commit();
				backupMng.dataChanged();
				return;
			}
			Log.i("ssid", "Current ssid is " + ssid);
			Profile profile = new Profile();
			if (profileStore.profileExistsforSSID(ssid))
				profile = profileStore.getProfilebySSID(ssid);
			else {
				notman.cancelAll();
				if (sp.getBoolean(MainActivity.new_ssid, true))
					set_newSsid_notification(ssid, context);
				editor.putString(lastRouterKey, ssid);
				editor.commit();
				backupMng.dataChanged();
				return;
			}
			if (!ssid.equals(sp.getString(lastRouterKey, ""))) {
				set_profile(profile);
				Intent onClickIntent = new Intent(context, MainActivity.class);
				onClickIntent.putExtra(NotificationService.notificationExtra, wifiNotiExtra);
				onClickIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
				PendingIntent pintent = PendingIntent.getActivity(context, 0, onClickIntent, PendingIntent.FLAG_CANCEL_CURRENT);
				NotificationService.set_notification(context, ssid, profile.getName(), "Wifi Profiles", ssid, pintent);
				editor.putString(lastRouterKey, ssid);
			}
		} else {
			notman.cancelAll();
			editor.remove(lastRouterKey);
		}
		editor.commit();
		backupMng.dataChanged();
		
		profileStore.close();
	}
	
	private void set_profile(Profile profile){
		if (profile.isSilent()) {
			Log.i("silent", "It's silent");
			am.setRingerMode(AudioManager.RINGER_MODE_SILENT);
		} else if (profile.isVibrate()) {
			Log.i("vibrate", "It's vibrate");
			am.setRingerMode(AudioManager.RINGER_MODE_VIBRATE);
		} else {
			Log.i("silent", "No silent");
			int ring = profile.getRinger_vol();
			am.setStreamVolume(AudioManager.STREAM_RING, ring, 0);
			int notvol = profile.getNotification_vol();
			am.setStreamVolume(AudioManager.STREAM_NOTIFICATION, notvol, 0);
		}
		int mediavol = profile.getMedia_vol();
		if (HeadsetPlugReceiver.connectedHeadphones) {
			HeadsetPlugReceiver.lastmediavol = mediavol;
		} else {
			am.setStreamVolume(AudioManager.STREAM_MUSIC, mediavol, 0);
			am.setStreamVolume(AudioManager.STREAM_SYSTEM, mediavol, 0);
		}
	}

	@SuppressLint("NewApi")
	private void set_newSsid_notification(String ssid, Context context) {
		if (!sp.getBoolean(MainActivity.noti_enabled_key, true))
    		return;
		
		Intent intent = new Intent(context, MainActivity.class);
		PendingIntent pintent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		if (android.os.Build.VERSION.SDK_INT < 16) {
			NotificationCompat.Builder notiBuilder = new NotificationCompat.Builder(context).setContentTitle("Wifi Profiles").setContentIntent(pintent)
					.setSmallIcon(com.sechristfamily.WifiProfiles.R.drawable.wifi).setContentText("No Profile Set for this SSID (" 
					+ ssid + ") Click to add one");

			notman = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			notman.notify(mId, notiBuilder.build());
		} else {
			Builder m_builder = new Notification.Builder(context);
		    m_builder.setContentTitle("Wifi Profiles")
		            .setContentText("No Profile Set for this SSID (" + ssid + ") Click to add one")
		            .setSmallIcon(com.sechristfamily.WifiProfiles.R.drawable.wifi)
		            .setContentIntent(pintent)
		            .setAutoCancel(true);
		    Notification m_notification = new Notification.BigTextStyle(m_builder)
		            .bigText("No Profile Set for this SSID (" + ssid + ") Click to add one")
		            .build();
	
			notman = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
			notman.notify(newssid_id, m_notification);
		}
	}
	
}
