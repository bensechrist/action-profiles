package com.WifiProfiles.Receivers;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.backup.BackupManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.NotificationCompat;

import com.WifiProfiles.MainActivity;
import com.sechristfamily.WifiProfiles.R;

public class ToggleServiceReceiver extends BroadcastReceiver {
	
	@Override
	public void onReceive(Context context, Intent intent) {
		NotificationManager notman = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		notman.cancelAll();
		SharedPreferences sp = context.getSharedPreferences(MainActivity.PrefsName, 0);
		Editor editor = sp.edit();
		editor.putBoolean(MainActivity.service_key, !sp.getBoolean(MainActivity.service_key, true));
		editor.commit();
		BackupManager backupMng = new BackupManager(context);
		backupMng.dataChanged();
		
		if (!sp.getBoolean(MainActivity.service_key, true))
			setReenableNoti(notman, context);
		else if (sp.getBoolean(MainActivity.noti_enabled_key, true)){
			Intent noti_enabled = new Intent(context, ProfileChangedReceiver.class);
			context.sendBroadcast(noti_enabled);
		}
	}
	
	public static void setReenableNoti(NotificationManager notman,
			Context context) {
		Intent intent = new Intent(context, MainActivity.class);
		PendingIntent pintent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_CANCEL_CURRENT);
		Intent enable = new Intent(context, ToggleServiceReceiver.class);
		PendingIntent enablepi = PendingIntent.getBroadcast(context, 0, enable, PendingIntent.FLAG_CANCEL_CURRENT);
		
		NotificationCompat.Builder builder = new NotificationCompat.Builder(context);
		builder.setContentTitle("Wifi Profiles").setContentIntent(pintent)
			.setSmallIcon(R.drawable.wifi).setContentText("WIFI PROFILES SERVICE DISABLED")
			.addAction(android.R.drawable.checkbox_off_background, "Enable Wifi Profiles", enablepi)
			.setOngoing(true);
		
		notman.notify(WifiStateChangeReceiver.mId, builder.build());
	}

}
