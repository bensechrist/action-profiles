package com.WifiProfiles.Receivers;

import java.util.List;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;

import com.WifiProfiles.MainActivity;
import com.WifiProfiles.Domain.Event;
import com.WifiProfiles.Stores.EventStore;
import com.WifiProfiles.Stores.ProfileStore;

public class StartAtBootReceiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		if("android.intent.action.BOOT_COMPLETED".equals(intent.getAction())) {
			SharedPreferences prefs = context.getSharedPreferences(MainActivity.PrefsName, 0);
			
			if (!prefs.getBoolean(MainActivity.service_key, true)) {
				return;
			}
			
			EventStore eventStore = new EventStore(context);
			eventStore.open();

			List<Event> events = eventStore.getEvents();
			long now = System.currentTimeMillis();
			if (!events.isEmpty()) {
				for (Event event : events) {
					int reoccurChoice = 0;
					long repeatTime = 0;
					long eventTime;
					if (event.getRecurring() != null) {
						if (event.getRecurring().equals("Every Day")) {
							reoccurChoice = 1;
							repeatTime = 86400000;
						} else if (event.getRecurring().equals("Every Other Day")) {
							reoccurChoice = 2;
							repeatTime = 86400000*2;
						} else if (event.getRecurring().equals("Every Week")) {
							reoccurChoice = 3;
							repeatTime = 604800000;
						} else if (event.getRecurring().equals("Every Other Week")) {
							reoccurChoice = 4;
							repeatTime = 604800000*2;
						}

						eventTime = event.getEventTimeInMills();
						while (eventTime < now) {
							eventTime += repeatTime;
						}
						
						setAlarm(context, reoccurChoice, eventTime, repeatTime, event);
					} else {
						if (!(event.getEventTimeInMills() < now)) {
							eventTime = event.getEventTimeInMills();
							setAlarm(context, reoccurChoice, eventTime, repeatTime, event);
						}
					}
				}
			}
			eventStore.close();
		}
	}

	private void setAlarm(Context context, int reoccurChoice, long eventTime, 
			long repeatTime, Event event) {
		AlarmManager alarmm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		ProfileStore profileStore = new ProfileStore(context);
		profileStore.open();

		Intent temp = new Intent(context, AlarmReceiver.class);
		temp.putExtra("profilename", profileStore.getProfilebyID(event.getIdToProfile()).getName());
		temp.putExtra("reoccur", (reoccurChoice > 0));
		temp.putExtra("eventTimeInMillis", eventTime);
		PendingIntent pintent_setProfile = PendingIntent.getBroadcast(context, event.getAlarmID(), 
				temp, PendingIntent.FLAG_CANCEL_CURRENT);

		if(reoccurChoice > 0){
			alarmm.setRepeating(AlarmManager.RTC_WAKEUP, event.getEventTimeInMills(), repeatTime, pintent_setProfile);
		} else {
			alarmm.set(AlarmManager.RTC_WAKEUP, event.getEventTimeInMills(), pintent_setProfile);
		}

		profileStore.close();
	}
}
