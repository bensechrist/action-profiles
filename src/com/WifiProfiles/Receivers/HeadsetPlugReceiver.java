package com.WifiProfiles.Receivers;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Log;

public class HeadsetPlugReceiver extends BroadcastReceiver{
	
	public static boolean connectedHeadphones = false;
	public static String headsetName;
	public static int lastmediavol = 0;
	private AudioManager am;

	@Override
	public void onReceive(Context context, Intent intent) {
		if (!intent.getAction().equals(Intent.ACTION_HEADSET_PLUG)) {
			return;
		}
		am = (AudioManager) context.getSystemService(Context.AUDIO_SERVICE);
		if (intent.getIntExtra("state", 0) == 0)
			am.setStreamVolume(AudioManager.STREAM_MUSIC, lastmediavol, 0);
		
		connectedHeadphones = (intent.getIntExtra("state", 0) == 1);
		headsetName = intent.getStringExtra("name");
		
		Log.i("Headphones", "Headphones are connected? " + String.valueOf(connectedHeadphones) + " Name of headphones? " + headsetName);
	}

}
