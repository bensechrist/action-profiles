package com.WifiProfiles.Receivers;

import java.util.Calendar;
import android.annotation.SuppressLint;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import com.WifiProfiles.Stores.EventStore;
import com.WifiProfiles.Stores.ProfileStore;

public class AddAlarmReceiver extends BroadcastReceiver {
	
	private AlarmManager alarmm;
	private ProfileStore profileStore;
	private EventStore eventStore;

	@SuppressLint("NewApi")
	@Override
	public void onReceive(Context context, Intent intent) {
		alarmm = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
		
		profileStore = new ProfileStore(context);
		profileStore.open();
		
		eventStore = new EventStore(context);
		eventStore.open();
		
		Bundle bundle = intent.getExtras();
		String profileName = bundle.getString("profileName");
		int year = bundle.getInt("year");
		int month = bundle.getInt("month");
		int day = bundle.getInt("day");
		int hour = bundle.getInt("hour");
		int minute = bundle.getInt("minute");
		Calendar eventTime = Calendar.getInstance();
		eventTime.set(year, month, day, hour, minute, 0);
		
		int reoccurChoice = bundle.getInt("reoccurTime");
		
		String recurrance = null;
		
		long repeatTime = 0;
		
		switch (reoccurChoice) {
		case 1:
			recurrance = "Every Day";
			repeatTime = 86400000;
			break;
			
		case 2:
			recurrance = "Every Other Day";
			repeatTime = 86400000*2;
			break;
			
		case 3:
			recurrance = "Every Week";
			repeatTime = 604800000;
			break;
			
		case 4:
			recurrance = "Every Other Week";
			repeatTime = 604800000*2;
			break;
		}
		
		long now = System.currentTimeMillis();
		long eventTimeMillis = eventTime.getTimeInMillis();
		if (reoccurChoice > 0) {
			while (eventTimeMillis < now) {
				eventTimeMillis += repeatTime;
			}
		}
		
		int alarmid = eventStore.addEvent(eventTimeMillis, 
				profileStore.getProfileID(profileStore.getProfilebyname(profileName)), recurrance);
		
		Intent temp = new Intent(context, AlarmReceiver.class);
		temp.putExtra("profilename", profileName);
		temp.putExtra("reoccur", (reoccurChoice > 0));
		temp.putExtra("eventTimeInMillis", eventTimeMillis);
		PendingIntent pintent_setProfile = PendingIntent.getBroadcast(context, alarmid, 
				temp, PendingIntent.FLAG_CANCEL_CURRENT);
		
		if(reoccurChoice > 0){
			if (android.os.Build.VERSION_CODES.KITKAT < android.os.Build.VERSION.SDK_INT)
				alarmm.setExact(AlarmManager.RTC_WAKEUP, eventTimeMillis, pintent_setProfile);
			else
				alarmm.setRepeating(AlarmManager.RTC_WAKEUP, eventTimeMillis, repeatTime, pintent_setProfile);
		} else {
			if (android.os.Build.VERSION_CODES.KITKAT < android.os.Build.VERSION.SDK_INT)
				alarmm.setExact(AlarmManager.RTC_WAKEUP, eventTimeMillis, pintent_setProfile);
			else
				alarmm.set(AlarmManager.RTC_WAKEUP, eventTimeMillis, pintent_setProfile);
		}
		
		Log.i("Alarm Adding", year + " " + month + " " + day + " " + hour + " " + minute + " " + repeatTime + " " + profileName);
		
		profileStore.close();
		eventStore.close();
	}

}
