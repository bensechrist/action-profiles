package com.WifiProfiles.Domain;

public class Location {

	private double latitude;
	private double longitude;
	private int profileEnterId;
	private int profileExitId;
	private String name;
	private float radius;
	
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getProfileEnterId() {
		return profileEnterId;
	}
	public void setProfileEnterId(int profileEnterId) {
		this.profileEnterId = profileEnterId;
	}
	public int getProfileExitId() {
		return profileExitId;
	}
	public void setProfileExitId(int profileExitId) {
		this.profileExitId = profileExitId;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public float getRadius() {
		return radius;
	}
	public void setRadius(float radius) {
		this.radius = radius;
	}
	
}
