package com.WifiProfiles.Domain;


public class Profile {
	
	private int Id;
	private String name;
	private String SSID;
	private int ringer_vol;
	private int notification_vol;
	private int media_vol;
	private boolean silent;
	private boolean vibrate;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getSSID() {
		return SSID;
	}
	public void setSSID(String sSID) {
		SSID = sSID;
	}
	public int getRinger_vol() {
		return ringer_vol;
	}
	public void setRinger_vol(int ringer_vol) {
		this.ringer_vol = ringer_vol;
	}
	public int getNotification_vol() {
		return notification_vol;
	}
	public void setNotification_vol(int notification_vol) {
		this.notification_vol = notification_vol;
	}
	public int getMedia_vol() {
		return media_vol;
	}
	public void setMedia_vol(int media_vol) {
		this.media_vol = media_vol;
	}
	public boolean isSilent() {
		return silent;
	}
	public void setSilent(boolean silent) {
		this.silent = silent;
	}
	public boolean isVibrate() {
		return vibrate;
	}
	public void setVibrate(boolean vibrate) {
		this.vibrate = vibrate;
	}
	public int getId() {
		return Id;
	}
	public void setId(int id) {
		Id = id;
	}
}
