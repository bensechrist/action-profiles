package com.WifiProfiles.Domain;

public class Event {

	private long eventTimeInMills;
	private int alarmID;
	private int idToProfile;
	private String recurring;
	
	public long getEventTimeInMills() {
		return eventTimeInMills;
	}
	public void setEventTimeInMills(long eventTimeInMills) {
		this.eventTimeInMills = eventTimeInMills;
	}
	public int getAlarmID() {
		return alarmID;
	}
	public String getRecurring() {
		return recurring;
	}
	public void setRecurring(String recurring) {
		this.recurring = recurring;
	}
	public void setAlarmID(int alarmID) {
		this.alarmID = alarmID;
	}
	public int getIdToProfile() {
		return idToProfile;
	}
	public void setIdToProfile(int idToProfile) {
		this.idToProfile = idToProfile;
	}
}
