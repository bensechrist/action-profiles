package com.WifiProfiles.Domain;

public class ConfiguredRouter {

	private String ssid;
	private Profile profile;
	
	public String getSsid() {
		return ssid;
	}
	
	public void setSsid(String ssid) {
		this.ssid = ssid;
	}
	
	public Profile getProfile() {
		return profile;
	}
	
	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == this) return true;
		if (o instanceof ConfiguredRouter)
			return ((ConfiguredRouter) o).getSsid().equals(ssid);
		return false;
		}
}
