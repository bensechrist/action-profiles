package com.WifiProfiles;

import java.util.ArrayList;
import java.util.List;

import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.WifiProfiles.Domain.Profile;
import com.WifiProfiles.Receivers.AddAlarmReceiver;
import com.WifiProfiles.Stores.ProfileStore;
import com.google.analytics.tracking.android.EasyTracker;
import com.sechristfamily.WifiProfiles.R;

public class AddDateTimeActivity extends Activity {

	@Override
	protected void onStart() {
		super.onStart();
		
		EasyTracker.getInstance(this).activityStart(this);
	}
	
	@Override
	protected void onStop() {
		super.onStop();
		
		EasyTracker.getInstance(this).activityStop(this);
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		int titleID = Resources.getSystem().getIdentifier("action_bar_title", "id", "android");
		
		TextView mAppName = (TextView) findViewById(titleID);
		mAppName.setTypeface(MainActivity.customFont);
		
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_add_date_time);
		
		ActionBar actionBar = getActionBar();
		actionBar.setDisplayHomeAsUpEnabled(true);
		
		com.WifiProfiles.Fonts.CustomButtonNormal submit = 
				(com.WifiProfiles.Fonts.CustomButtonNormal) findViewById(R.id.submit_date_time);
		com.WifiProfiles.Fonts.CustomButtonNormal cancel = 
				(com.WifiProfiles.Fonts.CustomButtonNormal) findViewById(R.id.cancel_date_time);
		final DatePicker datePicker = (DatePicker) findViewById(R.id.datepicker);
		final TimePicker timePicker = (TimePicker) findViewById(R.id.timepicker);
		final Spinner profileSpinner = (Spinner) findViewById(R.id.profile_spinner_date_time);
		final Spinner reoccurSpinner = (Spinner) findViewById(R.id.reoccur_spinner);
		
		ProfileStore profileStore = new ProfileStore(this);
		profileStore.open();
		List<Profile> profiles = profileStore.getProfiles();
		profileStore.close();
		List<String> profileNames = new ArrayList<String>();
		for (Profile profile : profiles) {
			profileNames.add(profile.getName());
		}
		if (profileNames.isEmpty()) {
			profileNames.add(MainActivity.NO_PROFILES);
		}
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, profileNames){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = super.getView(position, convertView, parent);
				
				((TextView) v).setTypeface(MainActivity.customFont);
				return v;
			}
			
			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {
				View v = super.getDropDownView(position, convertView, parent);
				
				((TextView) v).setTypeface(MainActivity.customFont);
				return v;
			}
		};
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		profileSpinner.setAdapter(adapter);
		
		String[] repeatChoices = getResources().getStringArray(R.array.reoccur_choices);
		ArrayAdapter<String> repeatAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_spinner_item, repeatChoices){
			@Override
			public View getView(int position, View convertView, ViewGroup parent) {
				View v = super.getView(position, convertView, parent);
				
				((TextView) v).setTypeface(MainActivity.customFont);
				return v;
			}
			
			@Override
			public View getDropDownView(int position, View convertView,
					ViewGroup parent) {
				View v = super.getDropDownView(position, convertView, parent);
				
				((TextView) v).setTypeface(MainActivity.customFont);
				return v;
			}
		};
		repeatAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		reoccurSpinner.setAdapter(repeatAdapter);
		
		submit.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				String profileName = profileSpinner.getSelectedItem().toString();
				
				if (profileName.equals(MainActivity.NO_PROFILES)) {
					Intent intent = new Intent(AddDateTimeActivity.this, AddProfile.class);
					intent.putExtra(AddProfile.mEXTRA_SSID, "no ssid");
					startActivity(intent);
					return;
				}
				
				Intent intent = new Intent(AddDateTimeActivity.this, AddAlarmReceiver.class);
				intent.putExtra("profileName", profileName);
				intent.putExtra("year", datePicker.getYear());
				intent.putExtra("month", datePicker.getMonth());
				intent.putExtra("day", datePicker.getDayOfMonth());
				intent.putExtra("hour", timePicker.getCurrentHour());
				intent.putExtra("minute", timePicker.getCurrentMinute());
				intent.putExtra("reoccurTime", reoccurSpinner.getSelectedItemPosition());
				sendBroadcast(intent);
				
				finish();
				
				Toast.makeText(AddDateTimeActivity.this, "Date Time Created Successfully", Toast.LENGTH_SHORT).show();
			}
		});
		
		cancel.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View v) {
				finish();
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case android.R.id.home:
			finish();
			break;

		}
		return super.onOptionsItemSelected(item);
	}
	
}
