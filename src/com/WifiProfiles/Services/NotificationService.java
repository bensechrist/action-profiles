package com.WifiProfiles.Services;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v4.app.NotificationCompat;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.style.StrikethroughSpan;

import com.WifiProfiles.MainActivity;
import com.WifiProfiles.Receivers.ToggleServiceReceiver;
import com.WifiProfiles.Receivers.WifiStateChangeReceiver;
import com.WifiProfiles.Stores.ProfileStore;

public class NotificationService {
	
	public static String notificationExtra = "notificationExtra";

	public static void set_notification(Context context, String newText, 
			String profileName, String title, String ssid, PendingIntent onClickIntent) {
		SharedPreferences prefs = context.getSharedPreferences(MainActivity.PrefsName, 0);
		
		NotificationManager notman = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
		
		if (!prefs.getBoolean(MainActivity.noti_enabled_key, true)) {
			notman.cancelAll();
			return;
		}
		
		Intent serintent = new Intent(context, ToggleServiceReceiver.class);
		PendingIntent serpintent = PendingIntent.getBroadcast(context, 0, serintent, PendingIntent.FLAG_UPDATE_CURRENT);
		
		NotificationCompat.Builder notiBuilder = new NotificationCompat.Builder(context).setContentTitle(title)
				.setContentIntent(onClickIntent).setSmallIcon(com.sechristfamily.WifiProfiles.R.drawable.wifi);
		notiBuilder.setContentText(profileName);
		notiBuilder.setContentInfo(ssid);
		notiBuilder.addAction(android.R.drawable.checkbox_on_background, "Disable Wifi Profiles", serpintent);
		notiBuilder.setOngoing(prefs.getBoolean(MainActivity.ongoingNotificationPref, true));
		
		String lastRouter = prefs.getString(WifiStateChangeReceiver.lastRouterKey, "");
		if (!lastRouter.equals("") && !lastRouter.equals(ssid)) {
			NotificationCompat.InboxStyle inboxStyle = new NotificationCompat.InboxStyle();
			ProfileStore profileStore = new ProfileStore(context);
			profileStore.open();
			SpannableString ss = new SpannableString(lastRouter + " -> " + profileStore.getProfilebySSID(lastRouter).getName());
			Spannable span = (Spannable) ss;
			span.setSpan(new StrikethroughSpan(), 0, ss.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
			inboxStyle.setBigContentTitle("Wifi Profiles")
				.addLine(span)
				.addLine(newText + " -> " + profileName);
			notiBuilder.setStyle(inboxStyle);
			profileStore.close();
		}
		
		notman.notify(WifiStateChangeReceiver.mId, notiBuilder.build());
	}
}
