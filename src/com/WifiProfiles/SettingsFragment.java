package com.WifiProfiles;

import android.app.NotificationManager;
import android.app.backup.BackupManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.preference.CheckBoxPreference;
import android.preference.PreferenceFragment;

import com.WifiProfiles.Receivers.ProfileChangedReceiver;
import com.WifiProfiles.Receivers.ToggleServiceReceiver;
import com.sechristfamily.WifiProfiles.R;

public class SettingsFragment extends PreferenceFragment implements OnSharedPreferenceChangeListener {

	private static BackupManager backupMng;
	private CheckBoxPreference servicePref;
	private CheckBoxPreference notiEnable;
	private CheckBoxPreference ongoingPref;
	private CheckBoxPreference newSsidPref;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		backupMng = new BackupManager(getActivity());
		
		getPreferenceManager().setSharedPreferencesName(MainActivity.PrefsName);
		addPreferencesFromResource(R.xml.settings);
		servicePref = (CheckBoxPreference) findPreference(MainActivity.service_key);
		notiEnable = (CheckBoxPreference) findPreference(MainActivity.noti_enabled_key);
		ongoingPref = (CheckBoxPreference) findPreference(MainActivity.ongoingNotificationPref);
		newSsidPref = (CheckBoxPreference) findPreference(MainActivity.new_ssid);
		notiEnable.setShouldDisableView(true);
		ongoingPref.setShouldDisableView(true);
		newSsidPref.setShouldDisableView(true);
		if (!servicePref.isChecked()) {
			notiEnable.setEnabled(false);
			ongoingPref.setEnabled(false);
			newSsidPref.setEnabled(false);
		}
		if (!notiEnable.isChecked()) {
			ongoingPref.setEnabled(false);
			newSsidPref.setEnabled(false);
		}
	}
	
	@Override
	public void onResume() {
		super.onResume();
		getPreferenceManager().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		getPreferenceManager().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
	}

	@Override
	public void onSharedPreferenceChanged(SharedPreferences prefs, String key) {
		switch (key) {
		case MainActivity.service_key:
			if (servicePref.isChecked()) {
				notiEnable.setEnabled(true);
				if (notiEnable.isChecked()) {
					ongoingPref.setEnabled(true);
					newSsidPref.setEnabled(true);
					Intent noti_enabled = new Intent(getActivity(), ProfileChangedReceiver.class);
					getActivity().sendBroadcast(noti_enabled);
				}
			} else {
				notiEnable.setEnabled(false);
				ongoingPref.setEnabled(false);
				newSsidPref.setEnabled(false);
				NotificationManager notman = 
						(NotificationManager) getActivity().getSystemService(Context.NOTIFICATION_SERVICE);
				ToggleServiceReceiver.setReenableNoti(notman, getActivity());
			}
			break;
		
		case MainActivity.noti_enabled_key:
			Intent noti_enabled = new Intent(getActivity(), ProfileChangedReceiver.class);
			getActivity().sendBroadcast(noti_enabled);
			if (notiEnable.isChecked()) {
				ongoingPref.setEnabled(true);
				newSsidPref.setEnabled(true);
			} else {
				ongoingPref.setEnabled(false);
				newSsidPref.setEnabled(false);
			}
			break;
			
		case MainActivity.ongoingNotificationPref:
			Intent changedOngoing = new Intent(getActivity(), ProfileChangedReceiver.class);
			getActivity().sendBroadcast(changedOngoing);
			break;
			
		default:
			break;
			
		}
		
		backupMng.dataChanged();
	}
	
}
